//
//  UITextField+Utils.h
//  Dapper
//
//  Created by Jignesh Chanchiya on 22/01/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITextField (Utils)

- (void)setPlaceholderColor:(UIColor *)color;

@end
