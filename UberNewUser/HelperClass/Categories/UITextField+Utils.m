//
//  UITextField+Utils.m
//  Dapper
//
//  Created by Jignesh Chanchiya on 22/01/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "UITextField+Utils.h"

@implementation UITextField (Utils)

- (void)setPlaceholderColor:(UIColor *)color {
    if (color) {
        [self setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    }
}

@end
