//
//  ProfileVC.h
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface ProfileVC : BaseVC <UITextFieldDelegate, UIActionSheetDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate,UIScrollViewDelegate,UIGestureRecognizerDelegate>
{
    UITapGestureRecognizer *tapTextHide;
}

- (IBAction)selectPhotoBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *proPicImgv;
@property (weak, nonatomic) IBOutlet UIScrollView *scrProfile;

@property (weak, nonatomic) IBOutlet UIButton *btnProPic;

@property (weak, nonatomic) IBOutlet UILabel *lblFirstName;
@property (weak, nonatomic) IBOutlet UILabel *lblLastName;

@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;

@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPhone;
@property (weak, nonatomic) IBOutlet UITextField *txtAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtBio;
@property (weak, nonatomic) IBOutlet UITextField *txtZipCode;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentPWD;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPWD;
@property (weak, nonatomic) IBOutlet UITextField *txtConformPWD;

@property (weak, nonatomic) IBOutlet UITextField *txtFullName;
@property (weak, nonatomic) IBOutlet UITextField *txtMainLastName;

@property (weak, nonatomic) IBOutlet UILabel *lblUpdateLnm;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdateProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectTimeZone;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *viewForPicker;
@property (weak, nonatomic) IBOutlet UILabel *lblSelectCountry;

- (IBAction)onClickBtnUpdate:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *btnEditProfile;

- (IBAction)onClickBtnEdit:(id)sender;
- (IBAction)selectTimeZone:(id)sender;
- (IBAction)pickerCacelBtn:(id)sender;
- (IBAction)pickerDonebtn:(id)sender;




@property (weak, nonatomic) IBOutlet UIButton *btnNavigation;

- (IBAction)btnEmailInfoClick:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailInfo;
@property (weak, nonatomic) IBOutlet UIView *viewForEmailInfo;
@end
