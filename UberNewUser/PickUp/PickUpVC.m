//
//  PickUpVC.m
//  UberNewUser
//
//  Created by Elluminati - macbook on 27/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "PickUpVC.h"
#import "SWRevealViewController.h"
#import "AFNHelper.h"
#import "AboutVC.h"
#import "ContactUsVC.h"
#import "ProviderDetailsVC.h"
#import "CarTypeCell.h"
#import "UIImageView+Download.h"
#import "CarTypeDataModal.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "UberStyleGuide.h"
#import "EastimateFareVC.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "UITextField+Utils.h"
#import "SelectPropsVC.h"
#import <GoogleMaps/GoogleMaps.h>



@interface PickUpVC ()
{//*strForCurLatitude,*strForCurLongitude
    NSString *strForUserId,*strForUserToken,*strForLatitude,*strForLongitude,*strForRequestID,*strForDriverLatitude,*strForDriverLongitude,*strForTypeid,*strMinFare,*strPassCap,*strETA,*Referral,*dist_price,*time_price,*driver_id,*strDestLatitude,*strDestLongitude;
    
    NSString *strSourceAddress, *strDestinationAddress;
    NSMutableArray *arrForInformation,*arrForApplicationType,*arrForAddress,*arrDriver,*arrType;
    NSMutableDictionary *driverInfo;
    NSArray *routes,*arrServiceType, *arrServiceText, *arrServiceGreenType;
    GMSMapView *mapView_;
    GMSMarker *source_marker,*destination_marker,*client_marker;
    GMSGeocoder *geocoder_;
    BOOL is_paymetCard,is_Fare,isPhoto;
    BOOL isSearch;
    double srclat,srclong,destlat,destlong;
}

@end

@implementation PickUpVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [APPDELEGATE hideLoadingView];
    //[super setNavBarTitle:NSLocalizedString(@"STEP 1 OF 3", nil)];
    [self checkRequestInProgress];
    [self SetLocalization];
    [self customSetup];
    [self getAllApplicationType];
    //[self getProviders];
    
    arrForApplicationType=[[NSMutableArray alloc]init];
    Referral=@"";
    isPhoto=0;
    strForTypeid=@"0";
    self.btnCancel.hidden=YES;
    arrForAddress=[[NSMutableArray alloc]init];
    self.tableForCity.hidden=YES;
    self.viewForPreferral.hidden=YES;
    self.viewForReferralError.hidden=YES;
    self.viewForTakePhoto.hidden=YES;
    self.viewForCreateRequest.hidden=YES;
    self.viewForNote.hidden=YES;
    self.viewForFareEstimate.hidden=YES;
    self.btnSubmitStep.hidden=YES;
    self.btnSkipStep.hidden=YES;
    self.viewForPromo.hidden=YES;
    self.viewForRateCard.hidden=YES;
    self.viewForSelectPayment.hidden=YES;
    is_Fare=NO;
    driverInfo=[[NSMutableDictionary alloc] init];
    self.viewForDriver.hidden=YES;
    [self.img_driver_profile applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [self customFont];
    
    CLLocationCoordinate2D coordinate = [self getLocation];
    //[self showMapCurrentLocatinn];
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    
    strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    if ([strForCurLongitude length]==0 || [strForCurLongitude isEqualToString:@""] || [strForCurLongitude isEqualToString:@"0.0"] || [strForCurLongitude intValue]==0 || [strForCurLongitude intValue]==0.000000)
    {
        /*if ([[pref valueForKey:@"current_lat"] length]==0 || [[pref valueForKey:@"current_lat"] isEqualToString:@""] || [[pref valueForKey:@"current_lat"] isEqualToString:@"0.0"] || [[pref valueForKey:@"current_lat"] intValue]==0 || [[pref valueForKey:@"current_lat"] intValue]==0.000000)
        {
            strForCurLatitude = [NSString stringWithFormat:@"0.000000"];
            strForCurLongitude = [NSString stringWithFormat:@"0.000000"];
        }
         
        else
         */
        {
            strForCurLatitude = [NSString stringWithFormat:@"%@",[pref valueForKey:@"current_lat"]];
            strForCurLongitude = [NSString stringWithFormat:@"%@",[pref valueForKey:@"current_long"]];
        }
    }
    
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLongitude;
    
    NSLog(@"current lat long = %@ %@",strForCurLatitude,strForCurLongitude);
    
    //[self getLocationFromString:strForLatitude Type:strForCurLongitude];
    
    [self getAddress:@"Current"];

    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForLatitude doubleValue] longitude:[strForLongitude doubleValue] zoom:15];
    
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    mapView_.delegate=self;
    
    /*client_marker = [[GMSMarker alloc] init];
    client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
    client_marker.map = mapView_;*/
    
    [self.viewGoogleMap addSubview:mapView_];
    [self.view bringSubviewToFront:self.tableForCity];
    [self myLocationPressed:self];
    /*arrServiceType = [NSArray arrayWithObjects:@"Van", @"Mini_Truck", @"Heavy_Truck",nil];
    arrServiceText = @[@"Van",@"Mini Truck",@"Heavy Truck"];
    arrServiceGreenType =  [NSArray arrayWithObjects:@"van_green",@"mini_truck_green",@"heavy_truck_green", nil];*/
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(KeyboardPickup:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate=self;
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.viewForNote addGestureRecognizer:tapGesture];
}

- (void)KeyboardPickup:(UIGestureRecognizer *)gst
{
    [self.txtPrmoCode resignFirstResponder];
    //[self.viewAmountResignview resignFirstResponder];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self getAllApplicationType];
    CLLocationCoordinate2D coordinate = [self getLocation];
    //[self showMapCurrentLocatinn];
    
    strForCurLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    strForCurLongitude= [NSString stringWithFormat:@"%f", coordinate.longitude];
    strForLatitude=strForCurLatitude;
    strForLongitude=strForCurLongitude;
    //[self getLocationFromString:strForLatitude Type:strForCurLongitude];
    
    [self getAddress:@"Current"];
    
    self.timerForGetProvider = [NSTimer
                                    scheduledTimerWithTimeInterval:5.0
                                    target:self
                                    selector:@selector(getProviders)
                                    userInfo:nil
                                    repeats:TRUE];
    
    //[self getProviders];

    if([[NSUserDefaults standardUserDefaults]boolForKey:@"isClear"] == NO)
    {
        self.txtAddress.text=@"";
        self.txtDestination.text=@"";
        [mapView_ clear];
        //[self getAddress:@"Current"];
    }
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    if([[pref valueForKey:PREF_IS_REFEREE] boolValue])
    {
        self.navigationController.navigationBarHidden=NO;
        // [self getAllApplicationType];
        [super setNavBarTitle:TITLE_PICKUP];
        [self customSetup];
        [self checkForAppStatus];
        [self getPagesData];
        [self.paymentView setHidden:YES];
        [self cashBtnPressed:nil];
    }
}
-(void)viewDidAppear:(BOOL)animated
{
    //[mapView_ clear];
    self.navigationController.navigationBarHidden=NO;
}
-(void)viewWillDisappear:(BOOL)animated
{
    self.navigationController.navigationBarHidden=NO;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.viewETA.hidden=YES;
    is_Fare=NO;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
         [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.revealViewController rightRevealToggle:nil];
    [self.txtNote resignFirstResponder];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,76,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
}
-(void)SetLocalization
{
    //[self.btnPickMeUp setTitle:NSLocalizedString(@"PICK ME UP", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnPayCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateNormal];
    [self.btnPayRequest setTitle:NSLocalizedString(@"Request", nil) forState:UIControlStateSelected];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateNormal];
    [self.btnSelService setTitle:NSLocalizedString(@"SELECT SERVICE YOU NEED", nil) forState:UIControlStateSelected];
    
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateNormal];
    [self.bReferralSkip setTitle:NSLocalizedString(@"SKIP", nil) forState:UIControlStateSelected];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateNormal];
    [self.bReferralSubmit setTitle:NSLocalizedString(@"ADD", nil) forState:UIControlStateSelected];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateNormal];
    [self.btnRatecard setTitle:NSLocalizedString(@"RATE CARD", nil) forState:UIControlStateSelected];
    
    self.lETA.text=NSLocalizedString(@"ETA", nil);
    self.lMaxSize.text=NSLocalizedString(@"MAX SIZE", nil);
    self.lMinFare.text=NSLocalizedString(@"MIN FARE", nil);
    self.lSelectPayment.text=NSLocalizedString(@"Select Your Payment Type", nil);
    self.lRefralMsg.text=NSLocalizedString(@"Referral_Msg", nil);
    self.lRate_basePrice.text=NSLocalizedString(@"Base Price :", nil);
    self.lRate_distancecost.text=NSLocalizedString(@"Distance Cost :", nil);
    self.lRate_TimeCost.text=NSLocalizedString(@"Time Cost :", nil);
    self.lblRateCradNote.text=NSLocalizedString(@"Rate_card_note", nil);
    self.txtAddress.placeholder=NSLocalizedString(@"Pick_up_location", nil);
    self.txtDestination.placeholder= NSLocalizedString(@"Delivery_Location", nil);
    self.txtPreferral.placeholder=NSLocalizedString(@"Enter Referral Code", nil);
    [self.txtPrmoCode setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
}
#pragma mark-
#pragma mark-
-(void)customFont
{
    self.txtAddress.font=[UberStyleGuide fontSemiBold:15.0f];
    self.txtDestination.font = [UberStyleGuide fontSemiBold:15.0f];
    self.btnCancel=[APPDELEGATE setBoldFontDiscriptor:self.btnCancel];
    //self.btnPickMeUp=[APPDELEGATE setBoldFontDiscriptor:self.btnPickMeUp];
    self.btnSelService=[APPDELEGATE setBoldFontDiscriptor:self.btnSelService];
    self.revealButtonItem.titleLabel.font = [UberStyleGuide fontRegularNav];
}
#pragma mark -
#pragma mark - Location Delegate
-(CLLocationCoordinate2D) getLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    return coordinate;
}
-(void)updateLocationManagerr
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    [locationManager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    // strForLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    //strForLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    // GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:newLocation.coordinate zoom:14];
    // [mapView_ animateWithCameraUpdate:updatedCamera];
    
    CLLocation *currentLocation = newLocation;
    
    if (currentLocation!=nil)
    {
        strForCurLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
        strForCurLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    }
    else
    {
        
    }
    
    CLLocationCoordinate2D current;
    current.latitude=[strForCurLongitude doubleValue];
    current.longitude=[strForCurLongitude doubleValue];
    
    mapView_.delegate=self;
    
    NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:[NSString stringWithFormat:@"%f",current.latitude ] forKey:@"current_lat"];
    [pref setObject:[NSString stringWithFormat:@"%f",current.longitude ] forKey:@"current_long"];
    [pref synchronize];
}

- (void)locationManager:(CLLocationManager *)manager
       didWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}
#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
}

#pragma mark- Google Map Delegate

- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position
{
    strForLatitude=[NSString stringWithFormat:@"%f",position.target.latitude];
    strForLongitude=[NSString stringWithFormat:@"%f",position.target.longitude];
    [self getAddress:@"Updated"];
}

- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
    /*CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(
                                                              self.map.centerCoordinate.latitude, self.map.centerCoordinate.longitude);*/
    //CLLocationCoordinate2D coordinate = [self getLocation];
    
    //[self showMapCurrentLocatinn];
}
-(void)getAddress:(NSString *)type
{
    if([type isEqualToString:@"source"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        NSArray *getLocation = [getLegs valueForKey:@"end_location"];
        
        srclat = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lat"] doubleValue];
        
        srclong = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lng"] doubleValue];
        
        if (getAddress.count!=0)
        {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        strSourceAddress = self.txtAddress.text;
    }
    if([type isEqualToString:@"destination"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        NSArray *getLocation = [getLegs valueForKey:@"end_location"];
        
        destlat = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lat"] doubleValue];
        
        destlong = [[[[getLocation objectAtIndex:0]objectAtIndex:0]valueForKey:@"lng"] doubleValue];
        
        if (getAddress.count!=0)
        {
            self.txtDestination.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        strDestinationAddress = self.txtDestination.text;
        
    }
    if ([type isEqualToString:@"Current"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForCurLatitude floatValue], [strForCurLongitude floatValue], [strForCurLatitude floatValue], [strForCurLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        
        if(getAddress.count != 0)
        {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        strSourceAddress = self.txtAddress.text;
        self.lblSourceAddress.text  = self.txtAddress.text;
    }
    if ([type isEqualToString:@"Updated"])
    {
        NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForLatitude floatValue], [strForLongitude floatValue], [strForLatitude floatValue], [strForLongitude floatValue]];
        
        NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
        
        NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                             options: NSJSONReadingMutableContainers
                                                               error: nil];
        NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
        NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
        NSArray *getAddress = [getLegs valueForKey:@"end_address"];
        
        if(getAddress.count != 0)
        {
            self.txtAddress.text=[[getAddress objectAtIndex:0]objectAtIndex:0];
        }
        strSourceAddress = self.txtAddress.text;
        self.lblSourceAddress.text  = self.txtAddress.text;
    }
    
}
#pragma mark -
#pragma mark - Mapview Delegate

-(void)showMapCurrentLocatinn
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coordinate zoom:10];
    [mapView_ animateWithCameraUpdate:updatedCamera];
    
    //[self getAddress:@"Current"];
}

-(void)mapView:(GMSMapView *)mapView didBeginDraggingMarker:(GMSMarker *)marker
{
    /*
     GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForCurLatitude doubleValue]
     longitude:[strForCurLongitude doubleValue]
     zoom:12];
     mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height) camera:camera];
     
     [mapView_ clear];
     
     */
    
    //    GMSMutablePath *pathpoliline=[GMSMutablePath path];
    //
    //    GMSPolyline *polyLinePath = [GMSPolyline polylineWithPath:pathpoliline];
    //
    //    [polyLinePath setMap:nil];
    
    //routes = nil;
}
-(void)mapView:(GMSMapView *)mapView didEndDraggingMarker:(GMSMarker *)marker
{
    //NSLog(@" marker = %@ ",marker.position.latitude);
    
    // CLLocationCoordinate2D coord =  CLLocationCoordinate2DMake(marker.position.latitude, marker.position.longitude);
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:marker.position.latitude longitude:marker.position.longitude];
    if(marker == source_marker)
    {
        [source_marker setUserData:marker];
        NSLog(@" s mark = %@",source_marker);
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:loc
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                           
                           if (error){
                               NSLog(@"Geocode failed with error: %@", error);
                               return;
                               
                           }
                           
                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           
                           NSDictionary *dictAddress = placemark.addressDictionary;
                           NSArray *arrFormatLine = [dictAddress valueForKey:@"FormattedAddressLines"];
                           NSString *data =@"";
                           for (NSString *address in arrFormatLine)
                           {
                               data = [data stringByAppendingString:address];
                               data = [data stringByAppendingString:@","];
                           }
                           NSLog(@"source data = %@",data);
                           self.txtAddress.text = data;
                           
                           CLLocation *locData = placemark.location;
                           srclat  = locData.coordinate.latitude;
                           srclong= locData.coordinate.longitude;
                           
                       }];
    }
    if(marker == destination_marker)
    {
        [destination_marker setUserData:marker];
        NSLog(@" s mark = %@",destination_marker);
        
        CLGeocoder *geocoder = [[CLGeocoder alloc] init];
        [geocoder reverseGeocodeLocation:loc
                       completionHandler:^(NSArray *placemarks, NSError *error) {
                           NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
                           
                           if (error){
                               NSLog(@"Geocode failed with error: %@", error);
                               return;
                               
                           }
                           
                           NSLog(@"placemarks=%@",[placemarks objectAtIndex:0]);
                           CLPlacemark *placemark = [placemarks objectAtIndex:0];
                           
                           NSDictionary *dictAddress = placemark.addressDictionary;
                           NSArray *arrFormatLine = [dictAddress valueForKey:@"FormattedAddressLines"];
                           NSString *data =@"";
                           for (NSString *address in arrFormatLine)
                           {
                               data = [data stringByAppendingString:address];
                               data = [data stringByAppendingString:@","];
                           }
                           NSLog(@" destination data = %@",data);
                           self.txtDestination.text = data;
                           
                           CLLocation *locData = placemark.location;
                           destlat  = locData.coordinate.latitude;
                           destlong= locData.coordinate.longitude;
                           
                       }];
        
    }
    
    
    
    CLLocationCoordinate2D sCoordinate;
    CLLocationCoordinate2D dCoordinate;
    
    
    sCoordinate = CLLocationCoordinate2DMake(srclat, srclong);
    dCoordinate = CLLocationCoordinate2DMake(destlat, destlong);
    
    //[self showRouteFrom:sCoordinate to:dCoordinate];
}
#pragma mark -
#pragma mark- Searching Method

- (IBAction)Searching:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    CLGeocoder *geocoder;
    
    NSString *str=self.txtAddress.text;
    UITextField  *txtSearch = (UITextField*)sender;
    
    if(txtSearch == self.txtAddress)
    {
        isSearch = true;
        NSLog(@"%@",str);
        
        if(str == nil)
            self.tableForCity.hidden=YES;
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        //[dictParam setObject:str forKey:PARAM_ADDRESS];
        [dictParam setObject:str forKey:@"input"]; // AUTOCOMPLETE API
        [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
        [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
                 
                 NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
                 
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     [self.tableForCity reloadData];
                     
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }
         }];
    }
}
-(IBAction)destSearching:(id)sender
{
    aPlacemark=nil;
    [placeMarkArr removeAllObjects];
    self.tableForCity.hidden=YES;
    CLGeocoder *geocoder;
    
    NSString *strDest = self.txtDestination.text;
    UITextField  *txtSearch = (UITextField*)sender;
    
    if(txtSearch == self.txtDestination)
    {
        isSearch = false;
        if(strDest == nil)
            self.tableForCity.hidden=YES;
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
        [dictParam setObject:strDest forKey:@"input"]; // AUTOCOMPLETE API
        [dictParam setObject:@"sensor" forKey:@"false"]; // AUTOCOMPLETE API
        [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getAddressFromGooglewAutoCompletewithParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 NSArray *arrAddress=[response valueForKey:@"predictions"]; //AUTOCOMPLTE API
                 
                 NSLog(@"AutoCompelete URL: = %@",[[response valueForKey:@"predictions"] valueForKey:@"description"]);
                 
                 if ([arrAddress count] > 0)
                 {
                     self.tableForCity.hidden=NO;
                     
                     placeMarkArr=[[NSMutableArray alloc] initWithArray:arrAddress copyItems:YES];
                     //[placeMarkArr addObject:Placemark]; o
                     [self.tableForCity reloadData];
                     
                     if(arrAddress.count==0)
                     {
                         self.tableForCity.hidden=YES;
                     }
                 }
             }
         }];
    }
}

#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
    
    cell.textLabel.text=formatedAddress;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    
    [self setNewPlaceData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(void)setNewPlaceData
{
    if(isSearch == true)
    {
        self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtAddress];
    }
    else
    {
        self.txtDestination.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
        [self textFieldShouldReturn:self.txtDestination];
    }
    
}

#pragma mark -
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if([segue.identifier isEqualToString:SEGUE_ABOUT])
    {
        AboutVC *obj=[segue destinationViewController];
        obj.arrInformation=arrForInformation;
    }
    else if([segue.identifier isEqualToString:SEGUE_TO_ACCEPT])
    {
        ProviderDetailsVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strForWalkStatedLatitude=strForDriverLatitude;
        obj.strForWalkStatedLongitude=strForDriverLongitude;
    }
    else if ([segue.identifier isEqualToString:SEGUE_TO_SELECT_PROPS])
    {
        SelectPropsVC *obj = [segue destinationViewController];
        obj.strGetSourceAddress = strSourceAddress;
        obj.strGetDestinationAddress = strDestinationAddress;
        obj.strForLatitude = strForLatitude;
        obj.strForLongitude = strForLongitude;
        obj.strForDestLatitude = strDestLatitude;
        obj.strForDestLongitude = strDestLongitude;
        obj.strForTypeId = strForTypeid;
        /*obj.strNote = [NSString stringWithFormat:@"%@",self.txtNote.text];
        obj.img = self.imgForRequest.image;*/
    }
    else if([segue.identifier isEqualToString:@"contactus"])
    {
        ContactUsVC *obj=[segue destinationViewController];
        obj.dictContent=sender;
    }
    else if ([segue.identifier isEqualToString:@"segueToEastimate"])
    {
        EastimateFareVC *obj=[segue destinationViewController];
        obj.strForLatitude=strForLatitude;
        obj.strForLongitude=strForLongitude;
        obj.strMinFare=strMinFare;
    }
}

-(void)goToSetting:(NSString *)str
{
    [self performSegueWithIdentifier:str sender:self];
}

#pragma mark -
#pragma mark - UIButton Action

- (IBAction)eastimateFareBtnPressed:(id)sender
{
    is_Fare=YES;
    self.viewForRateCard.hidden=YES;
    [self performSegueWithIdentifier:@"segueToEastimate" sender:nil];
}

- (IBAction)closeETABtnPressed:(id)sender
{
    self.viewETA.hidden=YES;
    self.viewForFareAddress.hidden=YES;
    self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
    is_Fare=NO;
}

- (IBAction)RateCardBtnPressed:(id)sender
{
    self.viewForRateCard.hidden=NO;
}


- (IBAction)ETABtnPressed:(id)sender {
    
    self.viewETA.hidden=NO;
    self.viewForRateCard.hidden=YES;
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateNormal];
    [self.btnFare setTitle:NSLocalizedString(@"GET FARE ESTIMATE", nil) forState:UIControlStateSelected];
}

- (IBAction)cashBtnPressed:(id)sender
{
    [self.btnCash setSelected:YES];
    [self.btnCard setSelected:NO];
    is_paymetCard=NO;
}

- (IBAction)cardBtnPressed:(id)sender
{
    [self.btnCash setSelected:NO];
    [self.btnCard setSelected:YES];
    is_paymetCard=YES;
}

- (IBAction)requestBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if ([strForTypeid isEqualToString:@"0"]||strForTypeid==nil)
        {
            strForTypeid=@"1";
        }
        if(![strForTypeid isEqualToString:@"0"])
        {
            if(((strForLatitude==nil)&&(strForLongitude==nil))
               ||(([strForLongitude doubleValue]==0.00)&&([strForLatitude doubleValue]==0)))
            {
                [APPDELEGATE showToastMessage:NSLocalizedString(@"NOT_VALID_LOCATION", nil)];
            }
            else
            {
                if([[AppDelegate sharedAppDelegate]connected])
                {
                    
                    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:strForLongitude  forKey:PARAM_LONGITUDE];

                    [dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];
                    if (is_paymetCard)
                    {
                        [dictParam setValue:@"0" forKey:PARAM_PAYMENT_OPT];
                    }
                    else
                    {
                        [dictParam setValue:@"1" forKey:PARAM_PAYMENT_OPT];
                    }
                    
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         
                         if (response)
                         {
                             self.paymentView.hidden=YES;
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"pick up......%@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                     //[self showDriver:walker];
                                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                     
                                     strForRequestID=[response valueForKey:@"request_id"];
                                     [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                     [self setTimerToCheckDriverStatus];
                                     
                                     [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                     [self.btnCancel setHidden:NO];
                                     [self.viewForDriver setHidden:NO];
                                     [APPDELEGATE.window addSubview:self.btnCancel];
                                     [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                     [APPDELEGATE.window addSubview:self.viewForDriver];
                                     [APPDELEGATE.window bringSubviewToFront:self.viewForDriver];
                                 }
                             }
                             else
                             {
                                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                 [alert show];
                             }
                         }
                     }];
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
        }
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> uHoo -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
}


#pragma mark-
#pragma mark- Image Controller methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"Media Info: %@", info);
    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
    
    UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    /*NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
    [pref setObject:photoTaken forKey:PARAM_DEVICE_PICTURE];
    [pref synchronize];*/
    self.imgForRequest.image=photoTaken;
    
    isPhoto=1;
    
    if(isPhoto==1)
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }
    else
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    UIAlertView *alert;
    
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:[error localizedDescription]
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
}

- (IBAction)onclickTakePhoto:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        
        [self presentModalViewController:imagePicker animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                       message:@"Unable to find a camera on your device."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
    /*if(isPhoto==1)
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }
    else
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }*/
}

- (IBAction)cancelBtnPressed:(id)sender
{
    //[self.paymentView setHidden:YES];
}

- (IBAction)onClickCloseRateView:(id)sender {
    
    self.viewForRateCard.hidden=YES;
}

- (IBAction)onClickApplyPromo:(id)sender {
    
    NSString *promoCode=self.txtPrmoCode.text;
    if(promoCode.length > 0)
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setValue:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setValue:promoCode forKey:PARAM_PROMO_CODE];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_APPLY_PROMO withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [[AppDelegate sharedAppDelegate]showToastMessage:@"Promo Code Has Been Added Successfully"];
                     }
                     else
                     {
                         [[AppDelegate sharedAppDelegate]showToastMessage:[response valueForKey:@"error"]];
                     }
                 }
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil message:NSLocalizedString(@"Please Promo", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }

}

- (IBAction)onClickCancelPromo:(id)sender {
    
    self.viewForPromo.hidden=YES;
    self.txtPrmoCode.text = @"";
}

- (IBAction)onClickRateCard:(id)sender {
    
    self.viewForRateCard.hidden=NO;
}

- (IBAction)onClickPromoCode:(id)sender {
    
    self.viewForPromo.hidden=NO;
}

- (IBAction)onClickFareEstimate:(id)sender {
    
    self.viewForFareEstimate.hidden=NO;
}

- (IBAction)onClickSelectPaymentBtn:(id)sender {
    
    self.viewForSelectPayment.hidden=NO;
    
}

- (IBAction)onClickBackPhoto:(id)sender {
    
    self.viewForNote.hidden=YES;
    self.viewForTakePhoto.hidden=NO;
    self.btnSubmitStep.hidden=YES;
    self.btnSkipStep.hidden=NO;
    self.txtNote.text=@"";
    self.imgForRequest.image=[UIImage imageNamed:@"no_photo"];
}

- (IBAction)onClickClosePhoto:(id)sender {
    
    self.viewForTakePhoto.hidden=YES;
    self.btnSkipStep.hidden = YES;
    self.viewForNote.hidden=YES;
    self.btnSubmitStep.hidden = YES;
}

- (IBAction)onClickSkipBtnStep:(id)sender
{
    self.viewForTakePhoto.hidden=YES;
    self.viewForNote.hidden=NO;
    self.btnSkipStep.hidden=YES;
    self.btnSubmitStep.hidden=NO;
    self.txtNote.text=@"";
    
    /*if(self.imgForRequest.image==nil)
        self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];*/
}

- (IBAction)onclickSubmitBtnStep:(id)sender
{
    if([self.txtNote.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Note First" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    else
        [self performSegueWithIdentifier:SEGUE_TO_SELECT_PROPS sender:nil];
    self.viewForNote.hidden=YES;
    self.btnSkipStep.hidden=YES;
    self.btnSubmitStep.hidden=YES;
    self.viewForTakePhoto.hidden=YES;
}

- (IBAction)pickMeUpBtnPressed:(id)sender
{
    self.btnPickMeUp.hidden=YES;
    self.viewForCreateRequest.hidden=NO;
    mapView_.frame = CGRectMake(0, 0, self.viewGoogleMap.frame.size.width, self.viewGoogleMap.frame.size.height-70);
    self.btnMyLocation.frame = CGRectMake(self.view.frame.size.width-40,self.view.frame.size.height-160,30,30);
}

- (IBAction)myLocationPressed:(id)sender
{
    if ([CLLocationManager locationServicesEnabled])
    {
        CLLocationCoordinate2D coor;
        coor.latitude=[strForCurLatitude doubleValue];
        coor.longitude=[strForCurLongitude doubleValue];
        GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:12];
        [mapView_ animateWithCameraUpdate:updatedCamera];
        /*source_marker = [[GMSMarker alloc] init];
        source_marker.position = CLLocationCoordinate2DMake([strForCurLatitude doubleValue], [strForCurLongitude doubleValue]);
        source_marker.icon=[UIImage imageNamed:@"pin_client_org"];
        [source_marker setDraggable:YES];
        source_marker.map = mapView_;*/
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> uHoo -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
}
- (IBAction)selectServiceBtnPressed:(id)sender
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        float closeY=(iOSDeviceScreenSize.height-self.btnSelService.frame.size.height);
        
        float openY=closeY-(self.bottomView.frame.size.height-self.btnSelService.frame.size.height);
        if (self.bottomView.frame.origin.y==closeY)
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, openY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
        }
        else
        {
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, closeY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
            } completion:^(BOOL finished)
             {
             }];
        }
    }
}

#pragma mark -
#pragma mark - Custom WS Methods

/*-(void)getALLWizards
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
    }
}*/

-(void)getAllApplicationType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableArray *arr=[[NSMutableArray alloc]init];
                     [arr addObjectsFromArray:[response valueForKey:@"types"]];
                     arrType=[response valueForKey:@"types"];
                     for(NSMutableDictionary *dict in arr)
                     {
                         CarTypeDataModal *obj=[[CarTypeDataModal alloc]init];
                         obj.id_=[dict valueForKey:@"id"];
                         obj.name=[dict valueForKey:@"name"];
                         obj.icon=[dict valueForKey:@"icon"];
                         obj.is_default=[dict valueForKey:@"is_default"];
                         obj.price_per_unit_time=[dict valueForKey:@"price_per_unit_time"];
                         obj.price_per_unit_distance=[dict valueForKey:@"price_per_unit_distance"];
                         obj.base_price=[dict valueForKey:@"base_price"];
                         obj.isSelected=NO;
                         obj.strImgType = [dict valueForKey:@"name"];
                         [arrForApplicationType addObject:obj];
                     }
                     [self.collectionView reloadData];
                 }
                 else
                 {}
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
-(void)setTimerToCheckDriverStatus
{
    if (timerForCheckReqStatus) {
        [timerForCheckReqStatus invalidate];
        timerForCheckReqStatus = nil;
    }
    
    timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatus) userInfo:nil repeats:YES];
}
-(void)checkForAppStatus
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    if(strReqId!=nil)
    {
        [self checkForRequestStatus];
    }
    else
    {
        [self RequestInProgress];
    }
}
-(void)checkForRequestStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                if([[response valueForKey:@"success"] intValue]==1)
                {
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                        {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         self.btnCancel.hidden=YES;
                         self.viewForDriver.hidden=YES;
                         
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             return ;
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                             
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [self.timerForGetProvider invalidate];
                             self.timerForGetProvider=nil;
                             
                             [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
                         }else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                     
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     
                     // [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     [APPDELEGATE hideLoadingView];
                     self.btnCancel.hidden=YES;
                     self.viewForDriver.hidden=YES;
                     // [self.btnCancel removeFromSuperview];
                     // [self showMapCurrentLocatinn];
                     
                 }
                 else
                 {
                     driverInfo=[response valueForKey:@"walker"];
                     //[self showDriver:driverInfo];
                 }
             }
             else
             {
                 if([[response valueForKey:@"error_code"] intValue]==406)
                     [APPDELEGATE checkLogIn];             }
            }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
/*
 -(void)checkDriverStatus
 {
 if([[AppDelegate sharedAppDelegate]connected])
 {
 // [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
 
 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
 strForUserId=[pref objectForKey:PREF_USER_ID];
 strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
 NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
 
 NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
 
 AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
 [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
 {
 if([[response valueForKey:@"success"]boolValue])
 {
 NSLog(@"GET REQ--->%@",response);
 NSString *strCheck=[response valueForKey:@"walker"];
 
 if(strCheck)
 {
 [timerForCheckReqStatus invalidate];
 timerForCheckReqStatus=nil;
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 [self performSegueWithIdentifier:SEGUE_TO_ACCEPT sender:self];
 }
 [[AppDelegate sharedAppDelegate]hideLoadingView];
 
 
 }
 else
 {}
 }];
 }
 else
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Status" message:@"Sorry, network is not available. Please try again later." delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
 [alert show];
 }
 }*/
-(void)checkRequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableDictionary *charge_details=[response valueForKey:@"charge_details"];
                     dist_price=[charge_details valueForKey:@"distance_price"];
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref setObject:dist_price forKey:PRFE_PRICE_PER_DIST];
                     time_price=[charge_details valueForKey:@"price_per_unit_time"];
                     [pref setObject:[charge_details valueForKey:@"price_per_unit_time"] forKey:PRFE_PRICE_PER_TIME];
                     //  [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     
                     self.lblRate_DistancePrice.text=[NSString stringWithFormat:@"$ %@",dist_price];
                     self.lblRate_TimePrice.text=[NSString stringWithFormat:@"$ %@",time_price];
                     
                     //[self checkForRequestStatus];
                 }
                 else
                 {
                     if([[response valueForKey:@"error_code"] intValue]==406)
                         [APPDELEGATE checkLogIn];
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
-(void)RequestInProgress
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_GET_REQUEST_PROGRESS,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                  
                     [pref setObject:[response valueForKey:@"request_id"] forKey:PREF_REQ_ID];
                     [pref synchronize];
                     [self checkForRequestStatus];
                 }
                 else
                 {
                     if([[response valueForKey:@"error_code"] intValue]==406)
                         [APPDELEGATE checkLogIn];
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getPagesData
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@",FILE_PAGE,PARAM_ID,strForUserId];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Request= %@",response);
             [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 arrPage=[response valueForKey:@"informations"];
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     //   [APPDELEGATE showToastMessage:@"Requset Accepted"];
                 }
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

-(void)getProviders
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    strForUserId=[pref objectForKey:PREF_USER_ID];
    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
    
    /*[dictParam setValue:strForUserId forKey:PARAM_ID];
    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
    [dictParam setValue:strForTypeid forKey:PARAM_TYPE];*/
    [dictParam setValue:strForLatitude forKey:@"usr_lat"];
    [dictParam setValue:strForLongitude forKey:@"user_long"];
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_GET_PROVIDERS withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"Respond to Get Provider= %@",response);
             // [APPDELEGATE hideLoadingView];
             
             if (response)
             {
                 // [arrDriver removeAllObjects];
                 arrDriver=[response valueForKey:@"walker_list"];
                 [self showProvider];
                 
             }
             else
             {
                 arrDriver=[[NSMutableArray alloc] init];
                 [self showProvider];
             }
             
         }];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];        [alert show];
    }
}
-(void)showProvider
{
    [mapView_ clear];
    
    BOOL is_first=YES;
    for (int i=0; i<arrDriver.count; i++)
    {
        NSDictionary *dict=[arrDriver objectAtIndex:i];
        NSString *strType=[NSString stringWithFormat:@"%@",[dict valueForKey:@"type"]];
        //if ([strForTypeid isEqualToString:strType])
        //{
            GMSMarker *driver_marker;
            driver_marker = [[GMSMarker alloc] init];
            driver_marker.map = nil;
            driver_marker.position = CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue],[[dict valueForKey:@"longitude"]doubleValue]);
            driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
            driver_marker.map = mapView_;
            
            if (is_first)
            {
                [self getETA:dict];
                is_first=NO;
            }
        //}
    }
    is_first=YES;
}

-(void)getETA:(NSDictionary *)dict
{
    CLLocationCoordinate2D scorr=CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    CLLocationCoordinate2D dcorr=CLLocationCoordinate2DMake([[dict valueForKey:@"latitude"]doubleValue], [[dict valueForKey:@"longitude"]doubleValue]);
    //[self calculateRoutesFrom:scorr to:dcorr];
    
}
#pragma mark - google map
-(void) showRouteFrom:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
    if(routes)
    {
        [mapView_ clear];
    }
    
    
    source_marker = [[GMSMarker alloc] init];
    source_marker.position=f;
    source_marker.icon = [UIImage imageNamed:@"pin_client_org"];
    source_marker.title = @"pick up";
    source_marker.map = mapView_;
    
    destination_marker = [[GMSMarker alloc] init];
    destination_marker.position = t;
    destination_marker.icon = [UIImage imageNamed:@"pin"];
    destination_marker.title = @"Delivery";
    destination_marker.map = mapView_;
    
    
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY_NEW];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
    GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
    singleLine.strokeWidth = 1.5f;
    singleLine.strokeColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0];
    singleLine.map = mapView_;
    
    routes = json[@"routes"];
    
    NSString *points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
    
    // TODO: better parsing. Regular expression?
    
    NSArray *temp= [self decodePolyLine:[points mutableCopy]];
    
    [self centerMap:temp];
    // create the
    
    
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded {
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\"
                                options:NSLiteralSearch
                                  range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init] ;
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len) {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5] ;
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5] ;
        printf("[%f,", [latitude doubleValue]);
        printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]] ;
        [array addObject:loc];
    }
    
    return array;
}


-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:10.0f]];
    
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrForApplicationType.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CarTypeCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cust" forIndexPath:indexPath];
    
    //[cell setCellData:[arrServiceType objectAtIndex:indexPath.row] setLable:[arrServiceText objectAtIndex:indexPath.row]];
    NSDictionary *dictType=[arrForApplicationType objectAtIndex:indexPath.row];
    
    if (strForTypeid==nil || [strForTypeid isEqualToString:@"0"])
    {
        if ([[dictType valueForKey:@"is_default"]intValue]==1)
        {
            for(CarTypeDataModal *obj in arrForApplicationType)
            {
                obj.isSelected = NO;
            }
            CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
            obj.isSelected = YES;
            cell.dataModel = obj;
            [self getETA:[arrDriver objectAtIndex:0]];
            NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
            strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
            strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
            self.lblETA.text=strETA;
            self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblRate_BasePrice.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblCarType.text=obj.name;
            self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
            strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
            [pref synchronize];
        }
    }
    [cell setCellData:[arrForApplicationType objectAtIndex:indexPath.row]];
    
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.txtAddress.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PLEASE_ENTER_PICKUP_LOCATION", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    /*else if (self.txtDestination.text.length == 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"PLEASE_ENTER_DELIVERY_LOCATION", nil) message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }*/
    else
    {
        
        for(CarTypeDataModal *obj in arrForApplicationType) {
            obj.isSelected = NO;
        }
        CarTypeDataModal *obj=[arrForApplicationType objectAtIndex:indexPath.row];
        obj.isSelected = YES;
        
        NSDictionary *dict=[arrType objectAtIndex:indexPath.row];
        strMinFare=[NSString stringWithFormat:@"%@",[dict valueForKey:@"min_fare"]];
        strPassCap=[NSString stringWithFormat:@"%@",[dict valueForKey:@"max_size"]];
        if ([strForTypeid intValue] !=[obj.id_ intValue])
        {
            // [self selectServiceBtnPressed:nil];
            self.lblETA.text=strETA;
            self.lblFare.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblRate_BasePrice.text=[NSString stringWithFormat:@"$ %@",strMinFare];
            self.lblCarType.text=obj.name;
            self.lblSize.text=[NSString stringWithFormat:@"%@ PERSONS",strPassCap];
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            [pref setObject:strMinFare forKey:PREF_FARE_AMOUNT];
            [pref synchronize];
        }
        strForTypeid=[NSString stringWithFormat:@"%@",obj.id_];
        [self.collectionView reloadData];
        
        // UIDevice *thisDevice=[UIDevice currentDevice];
        /*CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        float closeY=(iOSDeviceScreenSize.height-self.btnSelService.frame.size.height);
        
        float openY=closeY-(self.bottomView.frame.size.height-self.btnSelService.frame.size.height);
        if (self.bottomView.frame.origin.y==closeY)
        {
            
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, openY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
                
            } completion:^(BOOL finished)
             {
             }];
            
        }
        else
        {
            [UIView animateWithDuration:0.5 animations:^{
                
                self.bottomView.frame=CGRectMake(0, closeY, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
            } completion:^(BOOL finished)
             {
             }];
            
        }*/
        
        //[self performSegueWithIdentifier:SEGUE_TO_SELECT_PROPS sender:nil];
    }
}

#pragma mark
#pragma mark - UITextView Delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,-112,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,-112,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,76,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
}

#pragma mark
#pragma mark - UITextfield Delegate
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        self.txtAddress.text=@"";
        //  self.tableForCity.frame=CGRectMake(self.tableView.frame.origin.x, 86+34, self.tableForCity.frame.size.width, 132);
        self.tableForCity.frame=CGRectMake(28, 90, 260, 126);
    }
    if(textField ==  self.txtDestination)
    {
        self.txtDestination.text=@"";
        self.tableForCity.frame=CGRectMake(28, 90+40, 260, 126);
    }
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    //self.txtNote.text=@"";
    //self.txtNote.placeholder = @"Enter Your Notes Here";
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(0,-130,self.view.bounds.size.width,self.view.bounds.size.height);
    [UIView commitAnimations];

    if(textField == self.txtAddress)
    {
        client_marker.map=nil;
        source_marker.map=nil;
        [self getLocationFromString:self.txtAddress.text Type:@"source"];
    }
    if(textField == self.txtDestination)
    {
        destination_marker.map=Nil;
        [self getLocationFromString:self.txtDestination.text Type:@"destination"];
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.tableView.hidden=YES;
    
    if(textField==self.txtAddress)
    {
        [self.txtAddress resignFirstResponder];
    }
    else if (textField==self.txtDestination)
    {
        [self.txtDestination resignFirstResponder];
    }
    [textField resignFirstResponder];
    return YES;
}
-(void)getLocationFromString:(NSString *)str Type:(NSString *)type
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             CLLocationCoordinate2D source;
             CLLocationCoordinate2D coor;
             
             if ([arrAddress count] > 0)
             {
                 if([type isEqualToString:@"source"])
                 {
                     self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     
                     strForLatitude=[dictLocation valueForKey:@"lat"];
                     strForLongitude=[dictLocation valueForKey:@"lng"];
                     source.latitude=[strForLatitude doubleValue];
                     source.longitude=[strForLongitude doubleValue];
                     
                     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:source zoom:7];
                     [mapView_ animateWithCameraUpdate:updatedCamera];
                     
                     source_marker = [[GMSMarker alloc] init];
                     source_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
                     
                     source_marker.icon=[UIImage imageNamed:@"pin_client_org"];
                    // [source_marker setDraggable:YES];
                     source_marker.title = @"pick up";
                     source_marker.map = mapView_;
                     
                     [self getAddress:@"source"];
                 }
                 if([type isEqualToString:@"destination"])
                 {
                     self.txtDestination.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                     
                     NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                     strDestLatitude=[dictLocation valueForKey:@"lat"];
                     strDestLongitude=[dictLocation valueForKey:@"lng"];
                     
                     coor.latitude=[strDestLatitude doubleValue];
                     coor.longitude=[strDestLongitude doubleValue];
                     
                     GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:coor zoom:7];
                     [mapView_ animateWithCameraUpdate:updatedCamera];
                     destination_marker = [[GMSMarker alloc] init];
                     destination_marker.position = CLLocationCoordinate2DMake([strDestLatitude doubleValue], [strDestLongitude doubleValue]);
                     destination_marker.icon=[UIImage imageNamed:@"pin"];
                     destination_marker.map = mapView_;
                     //[destination_marker setDraggable:YES];
                     destination_marker.title=@"Delivery";
                     [self getAddress:@"destination"];
                 }
                 if ((self.txtAddress.text.length>0 && self.txtDestination.text.length>0))
                 {
                     CLLocationCoordinate2D cr;
                     cr.latitude=[strForLatitude doubleValue];
                     cr.longitude=[strForLongitude doubleValue];
                     CLLocationCoordinate2D r;
                     r.latitude=[strDestLatitude doubleValue];
                     r.longitude=[strDestLongitude doubleValue];
                     [self showRouteFrom:cr to:r];
                 }
             }
         }
     }];
}

#pragma mark -
#pragma mark - Referral btn Action

- (IBAction)btnSkipReferral:(id)sender
{
    Referral=@"1";
    [self createService];
}

- (IBAction)btnAddReferral:(id)sender
{
    Referral=@"0";
    [self createService];
}
-(void)createService
{
    self.viewForReferralError.hidden=YES;
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setObject:self.txtPreferral.text forKey:PARAM_REFERRAL_CODE];
        [dictParam setObject:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setObject:Referral forKey:PARAM_REFERRAL_SKIP];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_APPLY_REFERRAL withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSLog(@"%@",response);
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setObject:[response valueForKey:@"is_referee"] forKey:PREF_IS_REFEREE];
                         [pref synchronize];
                         self.viewForPreferral.hidden=YES;
                         self.btnMyLocation.hidden=NO;
                         self.btnETA.hidden=NO;
                         self.navigationController.navigationBarHidden=NO;
                         self.txtPreferral.text=@"";
                         if([Referral isEqualToString:@"0"])
                         {
                             [APPDELEGATE showToastMessage:[response valueForKey:@"error"]];
                         }
                         // [self setTimerToCheckDriverStatus];
                         self.navigationController.navigationBarHidden=NO;
                         [self getAllApplicationType];
                         [super setNavBarTitle:TITLE_PICKUP];
                         [self customSetup];
                         [self checkForAppStatus];
                         [self getPagesData];
                         // [self getProviders];
                         [self.paymentView setHidden:YES];
                         self.viewETA.hidden=YES;
                         [self cashBtnPressed:nil];
                     }
                 }
                 else
                 {
                     self.txtPreferral.text=@"";
                     self.viewForReferralError.hidden=NO;
                     self.lblReferralMsg.text=[response valueForKey:@"error"];
                     self.lblReferralMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                 }
             }
             
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}
#pragma mark -
#pragma mark - Memory Mgmt
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end

