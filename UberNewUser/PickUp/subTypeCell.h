//
//  subTypeCell.h
//  uHoo
//
//  Created by My Mac on 6/12/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface subTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSubTypeName;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTypePrise;
@property (weak, nonatomic) IBOutlet UILabel *lblPerMile;

@end
