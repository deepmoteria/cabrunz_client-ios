//
//  CarTypeCell.m
//  UberforXOwner
//
//  Created by Deep Gami on 14/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "CarTypeCell.h"
#import "CarTypeDataModal.h"
#import "UIImageView+Download.h"

@implementation CarTypeCell

-(void) setCellData:(NSString* )img setLable:(NSString *)lbl
{
    self.imgType.image=[UIImage imageNamed:img];
    self.lblTitle.text=lbl;
}

- (void)setCellData:(CarTypeDataModal *)data {
    cellData = data;
    self.dataModel = data;
    if (cellData.icon==nil || [cellData.icon isKindOfClass:[NSNull class]]){
        //self.imgType.image=[UIImage imageNamed:@"button_limo.png"];
    }
    else{
        if ([cellData.icon isEqualToString:@""]) {
            //self.imgType.image=[UIImage imageNamed:@"button_limo.png"];
        }
        else
        {
            //[self.imgType downloadFromURL:cellData.icon withPlaceholder:nil];
        }
    }
    self.lblTitle.text=cellData.name;
    if(cellData.isSelected)
    {
        self.imgCheck.hidden = NO;
        //self.lblTitle.font = [UberStyleGuide fontRegularBold:15.0f];
        self.lblTitle.textColor = [UIColor colorWithRed:48/255.0f green:106/255.0f blue:216/255.0f alpha:1];
    }
    else
    {
        self.imgCheck.hidden=YES;
        //self.lblTitle.font = [UberStyleGuide fontRegular:12.0f];
        self.lblTitle.textColor = [UIColor lightGrayColor];
    }
}
@end
