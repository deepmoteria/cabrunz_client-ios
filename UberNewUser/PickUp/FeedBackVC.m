//
//  FeedBackVC.m
//  UberNewUser
//
//  Created by Deep Gami on 01/11/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "UIImageView+Download.h"
#import "UIView+Utils.h"
#import "PickUpVC.h"
#import "UberStyleGuide.h"
#import "subTypeCell.h"

@interface FeedBackVC ()
{
    NSString *strPromo , *strReferral;
    NSMutableArray *arrNm;
    NSMutableDictionary *temp1;
}
@end

@implementation FeedBackVC

#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
   // [super setBackBarItem];
    
    [self SetLocalization];
    [self customFont];
  
    NSArray *arrName=[self.strFirstName componentsSeparatedByString:@" "];
    
    //self.lblFirstName.text=[arrName objectAtIndex:0];
    self.lblFirstName.text=self.strFirstName;
  //  self.lblLastName.text=[arrName objectAtIndex:1];
    
    self.lblDistance.textColor=[UberStyleGuide colorDefault];
    self.lblTIme.textColor=[UberStyleGuide colorDefault];
    //self.btnFeedBack.titleLabel.font = [UberStyleGuide fontRegularNav];

    self.lblDistance.text=[NSString stringWithFormat:@"%.2f",[[self.dictWalkInfo valueForKey:@"distance"] floatValue]];
    self.lblTIme.text=[self.dictWalkInfo valueForKey:@"time"];
    [self.imgUser applyRoundedCornersFullWithColor:[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1]];
    [self.imgUser downloadFromURL:self.strUserImg withPlaceholder:nil];
    
    self.viewForBill.hidden=NO;
    self.txtComments.text=NSLocalizedString(@"COMMENTS", nil);
    self.lblTotal.text=[NSString stringWithFormat:@"$ %.2f",[[dictBillInfo valueForKey:@"total"] floatValue]];

}
-(void)viewWillAppear:(BOOL)animated
{
    NSArray *arrTemp=[[NSArray alloc]initWithArray:self.arrSubTypes];
    self.arrSubTypes = [[NSMutableArray alloc]init];
    
    NSMutableDictionary *dictname = [[NSMutableDictionary alloc]init];
    [dictname setObject:NSLocalizedString(@"BASE_PRICE", nil) forKey:@"name"];
    [dictname setObject:[dictBillInfo valueForKey:@"base_price"] forKey:@"price"];
    [self.arrSubTypes addObject:dictname];
    
    NSMutableDictionary *dictServiceCost = [[NSMutableDictionary alloc]init];
    [dictServiceCost setObject:@"Service Cost"forKey:@"name"];
    [dictServiceCost setObject:[dictBillInfo valueForKey:@"service_cost"] forKey:@"price"];
    [self.arrSubTypes addObject:dictServiceCost];

    
    /*dictname = [[NSMutableDictionary alloc]init];
    [dictname setObject:NSLocalizedString(@"DISTANCE_COST", nil) forKey:@"name"];
    [dictname setObject:[dictBillInfo valueForKey:@"distance_cost"] forKey:@"price"];
   
    float totalDist=[[dictBillInfo valueForKey:@"distance_cost"] floatValue];
    float Dist=[[dictBillInfo valueForKey:@"distance"]floatValue];
    
    if(Dist!=0)
    {
       strPromo =[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
    }
    else
    {
        strPromo=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
    }
    [dictname setObject:strPromo forKey:@"distance_cost_per"];
    [self.arrSubTypes addObject:dictname];
    
    dictname = [[NSMutableDictionary alloc]init];
    [dictname setObject:NSLocalizedString(@"TIME_COST", nil) forKey:@"name"];
    [dictname setObject:[dictBillInfo valueForKey:@"time_cost"] forKey:@"price"];
    
    float totalTime=[[dictBillInfo valueForKey:@"time_cost"] floatValue];
    float Time=[[dictBillInfo valueForKey:@"time"]floatValue];
    if(Time!=0)
    {
        strPromo=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
    }
    else
    {
        strPromo=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
    }
    
    [dictname setObject:strPromo forKey:@"time_cost_per"];
    [self.arrSubTypes addObject:dictname];
    [self.arrSubTypes addObjectsFromArray:arrTemp];*/
    
    NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];

    [dictprice setObject:NSLocalizedString(@"PROMO_BONUS", nil) forKey:@"name"];
    [dictprice setObject:[dictBillInfo valueForKey:@"promo_bonus"] forKey:@"price"];
    [self.arrSubTypes addObject:dictprice];
    
    NSMutableDictionary *dictReferral = [[NSMutableDictionary alloc]init];
    [dictReferral setObject:NSLocalizedString(@"REFERRAL_BONUS", nil) forKey:@"name"];
    [dictReferral setObject:[dictBillInfo valueForKey:@"referral_bonus"] forKey:@"price"];
    [self.arrSubTypes addObject:dictReferral];
    
   
    
    
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Keyboard:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate=self;
    //[tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.view addGestureRecognizer:tapGesture];
}

- (void)Keyboard:(UIGestureRecognizer *)gst
{
    [self.txtComments resignFirstResponder];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.btnFeedBack setTitle:NSLocalizedString(@"Feedback", nil) forState:UIControlStateNormal];
}
-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    self.lComment.text=NSLocalizedString(@"COMMENT", nil);
    [self.btnConfirm setTitle:NSLocalizedString(@"CONFIRM", nil) forState:UIControlStateNormal];
    [self.btnSubmit setTitle:NSLocalizedString(@"Submit", nil) forState:UIControlStateNormal];
}


#pragma mark-
#pragma mark- Custom Font

-(void)customFont
{
    self.lblDistance.font=[UberStyleGuide fontRegular];
    self.lblDistCost.font=[UberStyleGuide fontRegular];
    self.lblBasePrice.font=[UberStyleGuide fontRegular];
    self.lblDistance.font=[UberStyleGuide fontRegular];
    self.lblPerDist.font=[UberStyleGuide fontRegular];
    self.lblPerTime.font=[UberStyleGuide fontRegular];
    self.lblTIme.font=[UberStyleGuide fontRegular];
    self.lblTimeCost.font=[UberStyleGuide fontRegular];
    self.lblTotal.font=[UberStyleGuide fontRegularBold:30.0f];
    self.lblFirstName.font=[UberStyleGuide fontRegularBold:17.0f];
    self.lblLastName.font=[UberStyleGuide fontRegular];
    //self.btnFeedBack.titleLabel.font=[UberStyleGuide fontRegular];
    self.lblPromoBouns.font=[UberStyleGuide fontRegular];
    self.lblRferralBouns.font=[UberStyleGuide fontRegular];
    self.txtComments.textColor = [UIColor whiteColor];
   // self.btnConfirm.titleLabel.font=[UberStyleGuide fontRegularBold];
   // self.btnSubmit.titleLabel.font=[UberStyleGuide fontRegularBold];
    //self.btnConfirm = [APPDELEGATE setBoldFontDiscriptor:self.btnConfirm];
    //self.btnSubmit = [APPDELEGATE setBoldFontDiscriptor:self.btnSubmit];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark-
#pragma makr- Btn Click Events

- (IBAction)submitBtnPressed:(id)sender
{
    [[NSUserDefaults standardUserDefaults]setBool:NO forKey:@"isClear"];
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REVIEWING", nil)];
        RBRatings rating=[ratingView getcurrentRatings];
        int rate=rating/2.0;
        if(rate==0)
        {
            [[AppDelegate sharedAppDelegate] hideLoadingView];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:NSLocalizedString(@"PLEASE_RATE", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];

        }
        else
        {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        [dictParam setObject:strForUserId forKey:PARAM_ID];
        [dictParam setObject:strForUserToken forKey:PARAM_TOKEN];
        [dictParam setObject:strReqId forKey:PARAM_REQUEST_ID];
        [dictParam setObject:[NSString stringWithFormat:@"%d",rate] forKey:PARAM_RATING];
        NSString *commt=self.txtComments.text;
        if([commt isEqualToString:NSLocalizedString(@"COMMENTS", nil)])
        {
            [dictParam setObject:@"" forKey:PARAM_COMMENT];
        }
        else
        {
            [dictParam setObject:self.txtComments.text forKey:PARAM_COMMENT];
        }

//        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RATE_DRIVER withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             NSLog(@"%@",response);
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"RATING", nil)];
                     [[NSUserDefaults standardUserDefaults] removeObjectForKey:PREF_REQ_ID];
                     
                     [self.navigationController popToRootViewControllerAnimated:YES];
                     
                     /*for (UIViewController *vc in self.navigationController.viewControllers)
                      {
                      if ([vc isKindOfClass:[PickUpVC class]])
                      {
                      [self.navigationController popToViewController:vc animated:YES];
                      return ;
                      }
                      }*/
                 }
             }
             
             [[AppDelegate sharedAppDelegate]hideLoadingView];

         }];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    

}
#pragma mark -
#pragma mark - UITextField Delegate


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, -150, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [UIView animateWithDuration:0.5 animations:^{
        
        self.view.frame=CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
        
        
    } completion:^(BOOL finished)
     {
     }];
  
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)confirmBtnPressed:(id)sender
{
    self.viewForBill.hidden=YES;
    ratingView=[[RatingBar alloc] initWithSize:CGSizeMake(160, 30) AndPosition:CGPointMake(85, 205)];
    ratingView.backgroundColor=[UIColor clearColor];
    [self.view addSubview:ratingView];

}

#pragma mark-
#pragma mark- Text Field Delegate

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.txtComments resignFirstResponder];
}



- (void)textViewDidBeginEditing:(UITextView *)textView
{
    
    self.txtComments.text=@"";
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                UITextPosition *beginning = [self.txtComments beginningOfDocument];
                [self.txtComments setSelectedTextRange:[self.txtComments textRangeFromPosition:beginning
                                                                                  toPosition:beginning]];
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, -210, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    
}
- (void)textViewDidEndEditing:(UITextView *)textView
{
    UIDevice *thisDevice=[UIDevice currentDevice];
    if(thisDevice.userInterfaceIdiom == UIUserInterfaceIdiomPhone)
    {
        CGSize iOSDeviceScreenSize = [[UIScreen mainScreen] bounds].size;
        
        if (iOSDeviceScreenSize.height == 568)
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 568);
                    
                } completion:^(BOOL finished) { }];
            }
        }
        else
        {
            if(textView == self.txtComments)
            {
                [UIView animateWithDuration:0.3 animations:^{
                    
                    self.view.frame = CGRectMake(0, 0, 320, 480);
                    
                } completion:^(BOOL finished) { }];
            }
        }
    }
    if ([self.txtComments.text isEqualToString:@""])
    {
        self.txtComments.text=NSLocalizedString(@"COMMENTS", nil);
    }
    
}

#pragma mark-
#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrSubTypes.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"subType";
    
    subTypeCell *cell = [self.tblForTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [self.arrSubTypes objectAtIndex:indexPath.row];
    cell.lblSubTypeName.text = [dict valueForKey:@"name"];
    cell.lblPerMile.hidden=YES;
    cell.lblSubTypePrise.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
    
    /*if(indexPath.row == 1 || indexPath.row == 2)
    {
        cell.lblPerMile.hidden = NO;
    }
    else
    {
        cell.lblPerMile.hidden = YES;
    }
    
    if(indexPath.row == 1)
    {
        cell.lblPerMile.text = [[self.arrSubTypes objectAtIndex:1]valueForKey:@"distance_cost_per"];
    }
    if(indexPath.row == 2)
    {
        cell.lblPerMile.text = [[self.arrSubTypes objectAtIndex:2]valueForKey:@"time_cost_per"];
    }*/
    return cell;
}
#pragma mark-
#pragma mark - Table view delegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
