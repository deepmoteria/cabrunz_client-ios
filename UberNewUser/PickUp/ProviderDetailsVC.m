//
//  ProviderDetailsVC.m
//  UberNewUser
//
//  Created by Deep Gami on 29/10/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "ProviderDetailsVC.h"
#import "SWRevealViewController.h"
#import "sbMapAnnotation.h"
#import "UIImageView+Download.h"
#import "FeedBackVC.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "Constants.h"
#import "RateView.h"
#import "UIView+Utils.h"
#import "UberStyleGuide.h"

@interface ProviderDetailsVC ()
{
    NSDate *dateForwalkStartedTime;
    //float distance;
    BOOL isTimerStaredForMin,isWalkInStarted,pathDraw;
    // NSMutableDictionary *dictBillInfo;
    NSMutableArray *arrPath , *arrSubTypes;
    GMSMutablePath *pathUpdates;
    NSString *strUSerImage,*strLastName;
    NSString *strProviderPhone,*strTime,*strDistance,*strForDestLat,*strForDestLong,*strForProviderLat,*strForProviderLong,*strForOwnerLat, *strForOwnerLong, *strDestinationLat , *strDestinationLong, *strETA,*strETAmin,*strETAvalue;
    GMSMapView *mapView_;
    GMSMarker *client_marker,*driver_marker, *destination_marker;
    GMSMutablePath *pathpoliline;
    NSDictionary *dictCard;
    BOOL iscash,isFirst;
}

@end
@implementation ProviderDetailsVC
@synthesize strForLongitude,strForLatitude,strForWalkStatedLatitude,strForWalkStatedLongitude,timerForTimeAndDistance,timerForCheckReqStatuss;
#pragma mark -
#pragma mark - View DidLoad

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [super setBackBarItem];
    [self SetLocalization];
    self.statusView.hidden=YES;
    self.tableForCity.hidden=YES;
    self.viewForPromo.hidden=YES;
    // strForLatitude=@"37.30000";
    // strForLongitude=@"-122.031";
    APPDELEGATE.vcProvider=self;
    [super setNavBarTitle:TITLE_PICKUP];
    [self customSetup];
    [self updateLocationManager];
    [self checkDriverStatus];
    
    // self.lblShowSourceAddress.text = self.strGetSourceAddress;
    //  self.lblShowDestinationAddress.text = self.strGetDestinationAddress;
    
    NSLog(@"arr :%@",self.arrForProps);
    
    arrPath=[[NSMutableArray alloc]init];
    pathUpdates = [GMSMutablePath path];
    pathUpdates = [[GMSMutablePath alloc]init];
    isTimerStaredForMin=NO;
    pathDraw=YES;
    isFirst=NO;
    
    if (strForLatitude.length==0)
    {
        strForLatitude = strForCurLatitude;
        strForLongitude = strForCurLongitude;
    }
    
    NSLog(@"lat long= %@  %@",strForLatitude,strForLongitude);
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForLatitude doubleValue] longitude:[strForLongitude doubleValue]zoom:12];
    mapView_=(GMSMapView*)[GMSMapView mapWithFrame:CGRectMake(0, 0,self.viewForMap.frame.size.width,self.viewForMap.frame.size.height) camera:camera];
    mapView_.myLocationEnabled = NO;
    [self.viewForMap addSubview:mapView_];
    [APPDELEGATE.window bringSubviewToFront:self.statusView];
    mapView_.delegate=self;
    
    // Creates a marker in the client Location of the map.
    /*client_marker = [[GMSMarker alloc] init];
    client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
    client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
    client_marker.map = mapView_;*/
    
    // Creates a marker in the client Location of the map.
    /*driver_marker = [[GMSMarker alloc] init];
    driver_marker.position = CLLocationCoordinate2DMake([strForDestLat doubleValue], [strForDestLong doubleValue]);
    driver_marker.icon=[UIImage imageNamed:@"pin"];
    driver_marker.map = mapView_;*/
    
    // Do any additional setup after loading the view.
    timerForCheckReqStatuss = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForTripStatus) userInfo:nil repeats:YES];
    isWalkInStarted=NO;
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    isWalkInStarted=[pref boolForKey:PREF_IS_WALK_STARTED];
    if(isWalkInStarted)
    {
        //[self requestPath];
    }
    
    self.acceptView.hidden=NO;
    self.lblStatus.text=@"Status :  Accepted the Job";
    
    [self customFont];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self checkForTripStatus];
}
-(void)viewDidAppear:(BOOL)animated
{
    //[self getAddress];
    
    //[self.btnCancel setHidden:NO];
    [self.ratingView initRateBar];
    [self.ratingView setUserInteractionEnabled:NO];
    //self.statusView.hidden=YES;
    // [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
    [self.imgForDriverProfile applyRoundedCornersFullWithColor:[UIColor colorWithRed:(98.0f/255.0f) green:(254.0f/255.0f) blue:(241.0f/255.0f) alpha:1.0]];
    self.tableForCity.hidden=YES;
    self.txtAddress.placeholder=NSLocalizedString(@"Destination Address", nil);
}
-(void)SetLocalization
{
    [self.btnCall setTitle:NSLocalizedString(@"CALL", nil) forState:UIControlStateNormal];
    self.lAcceptJob.text=NSLocalizedString(@"DRIVER ACCEPTED THE JOB", nil);
    self.lblWalkerArrived.text=NSLocalizedString(@"DRIVER HAS ARRIVED AT YOUR PLACE", nil);
    self.lblJobStart.text=NSLocalizedString(@"YOUR TRIP HAS BEEN STARTED", nil);
    self.lblWalkerStarted.text=NSLocalizedString(@"DRIVER HAS STARTED TOWARDS YOU", nil);
    self.lblJobDone.text=NSLocalizedString(@"YOUR TRIP IS COMPLETED", nil);
    self.lblAccept.text=NSLocalizedString(@"DRIVER ACCEPTED THE JOB", nil);
    self.lPromoCode.text=NSLocalizedString(@"Promo Code", nil);
    self.txtPromo.placeholder=NSLocalizedString(@"Enter Promo Code", nil);
    [self.btnPromoApply setTitle:NSLocalizedString(@"APPLY", nil) forState:UIControlStateNormal];
    [self.btnPromoApply setTitle:NSLocalizedString(@"APPLY", nil) forState:UIControlStateSelected];
    [self.btnPromoCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateNormal];
    [self.btnPromoCancel setTitle:NSLocalizedString(@"CANCEL", nil) forState:UIControlStateSelected];
    [self.btnPromoDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateNormal];
    [self.btnPromoDone setTitle:NSLocalizedString(@"Done", nil) forState:UIControlStateSelected];
    [self.btnCash setTitle:NSLocalizedString(@"Cash", nil) forState:UIControlStateNormal];
}
/*#pragma mark-
 #pragma mark- timer for oath draw
 
 -(void)setTimerToCheckDriverStatus
 {
 self.timerforpathDraw = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(setPathDrawBool) userInfo:nil repeats:YES];
 }
 */

#pragma mark-
#pragma mark- customFont
-(void)customFont
{
    /* self.lblDriverDetail.font=[UberStyleGuide fontRegular:13.0f];
    
     self.lblJobDone.font=[UberStyleGuide fontRegular:13.0f];
     self.lblJobStart.font=[UberStyleGuide fontRegular:13.0f];
     self.lblWalkerArrived.font=[UberStyleGuide fontRegular:13.0f];
     self.lblWalkerStarted.font=[UberStyleGuide fontRegular:13.0f];*/
    
    //self.lblAccept.font=[UberStyleGuide fontRegular];
    //self.lblAccept.textColor=[UIColor whiteColor];
     //self.lblDriverName.font=[UberStyleGuide fontRegularBold:17.0f];
    //self.lblRate.font = [UberStyleGuide fontRegularBold:17.0f];
    //self.btnCall.titleLabel.font = [UberStyleGuide fontRegular:13.0f];
    self.btnDistance.titleLabel.font = [UberStyleGuide fontRegular:13.0f];
    self.btnMin.titleLabel.font = [UberStyleGuide fontRegular:13.0f];
    
}
#pragma mark -
#pragma mark - Location Delegate

-(void)updateLocationManager
{
    [locationManager startUpdatingLocation];
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate=self;
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    
#ifdef __IPHONE_8_0
    if(SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"8")) {
        // Use one or the other, not both. Depending on what you put in info.plist
        //[self.locationManager requestWhenInUseAuthorization];
        [locationManager requestAlwaysAuthorization];
    }
#endif
    
    [locationManager startUpdatingLocation];
    
}
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    //strForLatitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude];
    // strForLongitude=[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    isWalkInStarted=[pref boolForKey:PREF_IS_WALK_STARTED];
    
    if(isWalkInStarted)
    {
        if (newLocation != nil) {
            if (newLocation.coordinate.latitude == oldLocation.coordinate.latitude && newLocation.coordinate.longitude == oldLocation.coordinate.longitude) {
                
            }
            else
            {
                [mapView_  clear];
                [pathUpdates addCoordinate:newLocation.coordinate];
                
                GMSPolyline *polyline = [GMSPolyline polylineWithPath:pathUpdates];
                polyline.strokeColor = [UIColor colorWithRed:(27.0f/255.0f) green:(151.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0];
                polyline.strokeWidth = 5.f;
                polyline.geodesic = YES;
                polyline.map = mapView_;
                
                // Creates a marker in the client Location of the map.
                
                GMSMarker *markerOwner = [[GMSMarker alloc] init];
                markerOwner.position = CLLocationCoordinate2DMake([strForOwnerLat doubleValue], [strForOwnerLong doubleValue]);
                markerOwner.icon = [UIImage imageNamed:@"pin_client_org"];
                markerOwner.map = mapView_;
                
                // Creates a marker in the client Location of the map.
                driver_marker = [[GMSMarker alloc] init];
                driver_marker.position = CLLocationCoordinate2DMake(newLocation.coordinate.latitude,newLocation.coordinate.longitude);
                driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
                //driver_marker.icon=[UIImage imageNamed:@"pin"];
                driver_marker.map = mapView_;
                
                if(pathpoliline.count!=0)
                {
                    /*GMSMarker *markerDriver = [[GMSMarker alloc] init];
                    markerDriver.position = CLLocationCoordinate2DMake([strForDestLat doubleValue], [strForDestLong doubleValue]) ;
                    markerDriver.icon = [UIImage imageNamed:@"pin_destination"];
                    markerDriver.map = mapView_;*/
                    
                    GMSPolyline *polyLinePath = [GMSPolyline polylineWithPath:pathpoliline];
                    
                    polyLinePath.strokeColor = [UIColor colorWithRed:(27.0f/255.0f) green:(151.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0];
                    polyLinePath.strokeWidth = 5.f;
                    polyLinePath.geodesic = YES;
                    polyLinePath.map = mapView_;
                    
                }
            }
        }
    }
    
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
    
    /*UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
     alertLocation.tag=100;
     [alertLocation show];
     */
}

#pragma mark-
#pragma mark- Alert Button Clicked Event

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if(alertView.tag==100)
    {
        if (buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
        }
    }
    
    if(alertView.tag==201)
    {
        if(buttonIndex == 1)
        {
            if([[AppDelegate sharedAppDelegate]connected])
            {
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         if([[response valueForKey:@"success"]boolValue])
                         {
                             is_walker_started=0;
                             is_walker_arrived=0;
                             is_started=0;
                             is_completed=0;
                             is_dog_rated=0;
                             
                             [timerForCheckReqStatuss invalidate];
                             timerForCheckReqStatuss=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             dictBillInfo = [response valueForKey:@"bill"];
                             strTime = [NSString stringWithFormat:@"%@",[[response valueForKey:@"request"] valueForKey:@"time"]];
                             
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                             [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:nil];
                         }
                     }
                     
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert show];
            }
        }
        else
        {
            return;
        }
    }
    
    if(alertView.tag==200)
    {
        if(buttonIndex == 1)
        {
            if([[AppDelegate sharedAppDelegate]connected])
            {
                [[AppDelegate sharedAppDelegate]hideLoadingView];
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
                [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
                
                AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                 {
                     if (response)
                     {
                         if([[response valueForKey:@"success"]boolValue])
                         {
                             
                             is_walker_started=0;
                             is_walker_arrived=0;
                             is_started=0;
                             is_completed=0;
                             is_dog_rated=0;
                             
                             [timerForCheckReqStatuss invalidate];
                             timerForCheckReqStatuss=nil;
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             
                             dictBillInfo = [response valueForKey:@"bill"];
                             strTime = [NSString stringWithFormat:@"%@",[[response valueForKey:@"request"] valueForKey:@"time"]];
                             
                             [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                             [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                             [self performSegueWithIdentifier:@"segueToHomeMap" sender:nil];
                         }
                      }
                     
                 }];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert show];
            }

        }
        else
        {
            
        }
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if (revealViewController)
    {
        [self.revealBtnItem addTarget:self.revealViewController action:@selector( revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        // [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}
-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    [self.revealViewController rightRevealToggle:nil];
}

#pragma mark -
#pragma mark - Mapview Delegate

-(void)showDriverLocatinOnMap
{
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForWalkStatedLatitude doubleValue] longitude:[strForWalkStatedLongitude doubleValue]zoom:14];
    mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 416) camera:camera];
    [self.viewForMap addSubview:mapView_];
    
    driver_marker = [[GMSMarker alloc] init];
    driver_marker.position = CLLocationCoordinate2DMake([strForWalkStatedLatitude doubleValue], [strForWalkStatedLongitude doubleValue]);
    driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
    driver_marker.map = mapView_;
  
}

-(void)showMapCurrentLocatin
{
    if([CLLocationManager locationServicesEnabled])
    {
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[strForLatitude doubleValue] longitude:[strForLongitude doubleValue]zoom:14];
        mapView_ = [GMSMapView mapWithFrame:CGRectMake(0, 0, 320, 416) camera:camera];
        mapView_.myLocationEnabled = NO;
        [self.viewForMap addSubview:mapView_];
  
        client_marker = [[GMSMarker alloc] init];
        client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
        client_marker.icon=[UIImage imageNamed:@"pin_client_org"];
        client_marker.map = mapView_;
     
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> Taxinow -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
    }
    
}

#pragma mark -
#pragma mark - Custom Methods

-(float)calculateDistanceFrom:(CLLocation *)locA To:(CLLocation *)locB
{
    CLLocationDistance distance;
    distance=[locA distanceFromLocation:locB];
    float Range=distance;
    return Range;
}
#pragma mark-
#pragma mark- Calculate Time & Distance

-(void)updateTime:(NSString *)starTime
{
    NSString *currentTime=[NSString stringWithFormat:@"%f",[[NSDate date] timeIntervalSince1970] * 1000];
    
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *datee = [df dateFromString:starTime];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    
    NSString *stratTime= [NSString stringWithFormat:@"%f",[datee timeIntervalSince1970] * 1000];
    
    double start = [stratTime doubleValue];
    double end=[currentTime doubleValue];
    
    NSTimeInterval difference = [[NSDate dateWithTimeIntervalSince1970:end] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:start]];
    
   // NSLog(@"difference: %f", difference);
    
    strTime=[NSString stringWithFormat:@"%f",(difference/(1000*60))];
    
    if(strTime==0)
    {
        strTime=@"1";
    }
    
    [self.btnMin setTitle:[NSString stringWithFormat:@"%@ %@",strTime,NSLocalizedString(@"Mins", nil)] forState:UIControlStateNormal];
    [self.btnMin setTitle:[NSString stringWithFormat:@"%@ %@",strTime,NSLocalizedString(@"Mins", nil)] forState:UIControlStateHighlighted];
    NSLog(@"difrance = %@",strTime);
    
    /*
    NSString *gmtDateString = starTime;
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
    NSDate *datee = [df dateFromString:gmtDateString];
    df.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:[NSTimeZone localTimeZone].secondsFromGMT];
    
    double dateTimeDiff=  [[NSDate date] timeIntervalSince1970] - [datee timeIntervalSince1970];
    int Diff=(dateTimeDiff/60)-2;
    if(Diff<=2)
    {
        Diff=0;
    }
    strTime=[NSString stringWithFormat:@"%d %@",Diff,NSLocalizedString(@"Min", nil)];
    [self.btnMin setTitle:[NSString stringWithFormat:@"%d %@",Diff,NSLocalizedString(@"Min", nil)] forState:UIControlStateNormal];
    [self.btnMin setTitle:[NSString stringWithFormat:@"%d %@",Diff,NSLocalizedString(@"Min", nil)] forState:UIControlStateHighlighted];
    NSLog(@"Min %d",Diff);
    */
}

-(void)cancelRequest
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        //[[AppDelegate sharedAppDelegate]hideLoadingView];
        //[[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
        
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [timerForCheckReqStatuss invalidate];
                     timerForCheckReqStatuss=nil;
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     
                     dictBillInfo = [response valueForKey:@"bill"];
                     strTime = [NSString stringWithFormat:@"%@",[[response valueForKey:@"request"] valueForKey:@"time"]];
                     
                     //after added bill and status
                     
                     if(is_walker_started==0)
                     {
                         is_walker_arrived=0;
                         is_walker_started=0;
                         is_completed=0;
                         is_started=0;
                         is_dog_rated=0;
                         
                         [USERDEFAULT removeObjectForKey:PREF_REQ_ID];
                         //[APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         [self performSegueWithIdentifier:@"segueToHomeMap" sender:nil];
                     }
                     else
                     {
                         is_walker_arrived=0;
                         is_walker_started=0;
                         is_completed=0;
                         is_started=0;
                         is_dog_rated=0;
                         
                         //[APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:nil];
                     }
                     
                     /*is_walker_arrived=0;
                      is_walker_started=0;
                      is_completed=0;
                      is_started=0;
                      is_dog_rated=0;
                      
                      [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                      [self performSegueWithIdentifier:@"segueToHomeMap" sender:nil];*/
                     //[self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [alert show];
                 }
             }
             
         }];
    }
}

-(void)checkForTripStatus
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"GET REQ--->%@",response);
             if (response)
             {
                 if([[response valueForKey:@"is_cancelled"]integerValue]==1)
                 {
                     [self cancelRequest];
                 }
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     NSMutableDictionary *dictOwner=[response valueForKey:@"owner"];
                     NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                     arrSubTypes = [[response valueForKey:@"type"] mutableCopy];
                     
                     self.lblRateValue.text=[NSString stringWithFormat:@"%.1f",[[dictWalker valueForKey:@"rating"] floatValue]];
                     
                     RBRatings rate=([[dictWalker valueForKey:@"rating"]floatValue]*2);
                     [self.ratingView setRatings:rate];
                     
                     //strLastName=[dictWalker valueForKey:@"last_name"];
                     self.lblDriverName.text=[dictWalker valueForKey:@"full_name"];
                     self.lblDriverbio.text=[dictWalker valueForKey:@"bio"];
                     self.lblDriverDetail.text=[dictWalker valueForKey:@"phone"];
                     strProviderPhone=[NSString stringWithFormat:@"%@",[dictWalker valueForKey:@"phone"]];
                     [self.imgForDriverProfile downloadFromURL:[dictWalker valueForKey:@"picture"] withPlaceholder:nil];
                     strUSerImage=[dictWalker valueForKey:@"picture"];
                     
                     self.lblCarNum.text=[dictWalker valueForKey:@"car_number"];
                     self.lblCarType.text=[dictWalker valueForKey:@"car_model"];
                     self.lblRate.text=[NSString stringWithFormat:@"%.2f",[[dictWalker valueForKey:@"rating"]floatValue]];
                     dictCard=[response valueForKey:@"card_details"];
                     strDestinationLat=[dictWalker valueForKey:@"latitude"];
                     strDestinationLong=[dictWalker valueForKey:@"longitude"];
                     strForOwnerLat = [dictOwner valueForKey:@"owner_lat"];
                     strForOwnerLong = [dictOwner valueForKey:@"owner_long"];
                     //[self getAddress];
                     
                     CLLocationCoordinate2D clientLatLong;
                     clientLatLong.latitude=[strForOwnerLat doubleValue];
                     clientLatLong.longitude=[strForOwnerLong doubleValue];
                     CLLocationCoordinate2D driverLatLong;
                     driverLatLong.latitude=[strDestinationLat doubleValue];
                     driverLatLong.longitude=[strDestinationLong doubleValue];
                     
                     [self calculateRoutesFrom:clientLatLong to:driverLatLong];
                     
                     UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,70,70)];
                     //view.backgroundColor = [UIColor redColor];

                     UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin_ETA"]];
                     
                     UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13, 4, 30, 30)];
                     label.backgroundColor = [UIColor greenColor];
                     label.text = strETAmin;
                     label.textColor = [UIColor blueColor];
                     label.textAlignment = NSTextAlignmentCenter;
                     label.font = [UberStyleGuide fontRegularBold:12.0f];
                     
                     UILabel *labelVal = [[UILabel alloc]initWithFrame:CGRectMake(8, 14, 30, 30)];
                     labelVal.text = strETAvalue;
                     labelVal.textColor = [UIColor whiteColor];
                     labelVal.textAlignment = NSTextAlignmentCenter;
                     labelVal.font = [UberStyleGuide fontRegularBold:9.0f];
                     [label sizeToFit];
                     [labelVal sizeToFit];
                     [view addSubview:pinImageView];
                     [view addSubview:label];
                     [view addSubview:labelVal];
                     
                    if(isFirst==NO)
                     {
                         [mapView_ clear];
                         NSMutableDictionary *dictCharge=[response valueForKey:@"charge_details"];
                         
                         strForLatitude=[dictOwner valueForKey:@"owner_lat"];
                         strForLongitude=[dictOwner valueForKey:@"owner_long"];
                         
                        // client_marker.position = CLLocationCoordinate2DMake([strForLatitude doubleValue], [strForLongitude doubleValue]);
                        // client_marker.map = mapView_;
                         
                         [self calculateRoutesFrom:clientLatLong to:driverLatLong];
                         
                         isFirst=YES;
                         
                         UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,70,70)];
                         UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin_ETA"]];
                         
                         UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13, 4, 30, 30)];
                         label.text = strETAmin;
                         label.font = [UberStyleGuide fontRegularBold:12.0f];
                         label.textColor = [UIColor whiteColor];
                         label.textAlignment = NSTextAlignmentCenter;
                         
                         UILabel *labelVal = [[UILabel alloc]initWithFrame:CGRectMake(8,14, 30, 30)];
                         labelVal.text = strETAvalue;
                         labelVal.textColor = [UIColor whiteColor];
                         labelVal.textAlignment = NSTextAlignmentCenter;
                         labelVal.font = [UberStyleGuide fontRegularBold:9.0f];
                         [label sizeToFit];
                         [labelVal sizeToFit];
                         
                         [view addSubview:pinImageView];
                         [view addSubview:label];
                         [view addSubview:labelVal];

                         UIImage *markerIcon = [self imageFromView:view];
                         client_marker.icon = markerIcon;
                         client_marker.map = mapView_;
                         
                         /*destination_marker.position = CLLocationCoordinate2DMake([strDestinationLat doubleValue], [strDestinationLong doubleValue]);
                         destination_marker.icon = [UIImage imageNamed:@"pin"];
                         destination_marker.map = mapView_;*/
                         
                         driver_marker.position = CLLocationCoordinate2DMake([strForWalkStatedLatitude doubleValue], [strForWalkStatedLongitude doubleValue]);
                         driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
                         driver_marker.map = mapView_;
                         
                         CLLocationCoordinate2D clientCoStr;
                         clientCoStr.latitude=[strForLatitude doubleValue];
                         clientCoStr.longitude=[strForLongitude doubleValue];
                         CLLocationCoordinate2D driverCoStr;
                         driverCoStr.latitude=[strDestinationLat doubleValue];
                         driverCoStr.longitude=[strDestinationLong doubleValue];
                         
                         [self showRouteFrom:clientCoStr to:driverCoStr];
                         
                         [self centerMapFirst:clientCoStr two:driverCoStr third:driverCoStr];
                     }
                     else
                     {
                         
                     }
                     
                     is_walker_started=[[response valueForKey:@"is_walker_started"] intValue];
                     is_walker_arrived=[[response valueForKey:@"is_walker_arrived"] intValue];
                     is_started=[[response valueForKey:@"is_walk_started"] intValue];
                     is_completed=[[response valueForKey:@"is_completed"] intValue];
                     is_dog_rated=[[response valueForKey:@"is_walker_rated"] intValue];
                     
                     strDistance=[NSString stringWithFormat:@"%.2f %@",[[response valueForKey:@"distance"] floatValue],[response valueForKey:@"unit"]];
                     [self checkDriverStatus];
                     if(!isWalkInStarted)
                     {
                         driver_marker.map=nil;
                         driver_marker = [[GMSMarker alloc] init];
                         driver_marker.position = CLLocationCoordinate2DMake([[dictWalker valueForKey:@"latitude"] doubleValue], [[dictWalker valueForKey:@"longitude"] doubleValue]);
                         driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
                         driver_marker.map = mapView_;
                     }
                     
                     if(is_completed==1)
                     {
                         [self updateTime:[response valueForKey:@"start_time"]];
                         isWalkInStarted=NO;
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
                         
                         dictBillInfo=[response valueForKey:@"bill"];
                         
                         FeedBackVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[FeedBackVC class]])
                             {
                                 vcFeed = (FeedBackVC *)vc;
                             }
                             
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatuss invalidate];
                             [timerForTimeAndDistance invalidate];
                             timerForTimeAndDistance=nil;
                             timerForCheckReqStatuss=nil;
                             [self.timerforpathDraw invalidate];
                             
                             [self performSegueWithIdentifier:SEGUE_TO_FEEDBACK sender:self];
                         }
                         else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                         
                     }
                     /*else if(is_walker_arrived==1)
                     {
                         //[self.btnCancel setHidden:YES];
                     }*/

                     else if(is_started==1)
                     {
                         
                         //[self setTimerToCheckDriverStatus];
                         [locationManager startUpdatingLocation];
                         [self updateTime:[response valueForKey:@"start_time"]];
                         [self.btnDistance setTitle:[NSString stringWithFormat:@"%.1f %@",[[response valueForKey:@"distance"] floatValue],[response valueForKey:@"unit"]] forState:UIControlStateNormal];
                         
                         isWalkInStarted=YES;
                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                         [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
                         
                         if(isTimerStaredForMin==NO)
                         {
                             isTimerStaredForMin=YES;
                             // [self checkTimeAndDistance];
                             dateForwalkStartedTime=[NSDate date];
                             // timerForTimeAndDistance= [NSTimer scheduledTimerWithTimeInterval:60.0 target:self selector:@selector(checkTimeAndDistance) userInfo:nil repeats:YES];
                         }
                         strForWalkStatedLatitude=[dictWalker valueForKey:@"latitude"];
                         strForWalkStatedLongitude=[dictWalker valueForKey:@"longitude"];
                     }
                     strForWalkStatedLatitude=[dictWalker valueForKey:@"latitude"];
                     strForWalkStatedLongitude=[dictWalker valueForKey:@"longitude"];
                 }
                 else
                 {
                 }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
        
    }
}

-(void)requestPath
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSString *strForUserId=[pref objectForKey:PREF_USER_ID];
    NSString *strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
    NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
    
    
    NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_REQUEST_PATH,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
     {
         
         NSLog(@"Page Data= %@",response);
         if (response)
         {
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [arrPath removeAllObjects];
                 arrPath=[response valueForKey:@"locationdata"];
                 [self drawPath];
             }
         }
         
     }];
}
-(int)checkDriverStatus
{
    if(is_walker_started==1)
    {
        [self.btnWalkerStart setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        self.viewForStarted.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblStatus.text=NSLocalizedString(@"DRIVER HAS STARTED TOWARDS YOU", nil);
        self.lblStatus.textColor=[UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblAccept.text=NSLocalizedString(@"IS_WLAKER", nil);
        self.lblWalkerStarted.textColor=[UIColor whiteColor];
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
        self.acceptView.hidden=NO;
        self.statusView.hidden=YES;
    }
    else
    {
        [self.btnWalkerStart setBackgroundImage:[UIImage imageNamed:Nil] forState:UIControlStateNormal];
        //self.lblWalkerStarted.textColor=[UIColor darkGrayColor];
        self.lblStatus.textColor = [UIColor whiteColor];
        self.viewForStarted.backgroundColor = [UIColor clearColor];
    }
    
    if(is_walker_arrived==1)
    {
        [self.btnWalkerArrived setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        self.viewForArrived.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblStatus.text=NSLocalizedString(@"DRIVER HAS ARRIVED AT YOUR PLACE", nil);
        self.lblStatus.textColor=[UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblWalkerArrived.textColor=[UIColor whiteColor];
        self.lblAccept.text=NSLocalizedString(@"IS_ARRIVED", nil);;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
        self.acceptView.hidden=NO;
        self.statusView.hidden=YES;
    }
    else
    {
        [self.btnWalkerArrived setBackgroundImage:[UIImage imageNamed:Nil] forState:UIControlStateNormal];
        //self.lblWalkerArrived.textColor=[UIColor darkGrayColor];
        self.lblStatus.textColor = [UIColor whiteColor];
        self.viewForArrived.backgroundColor = [UIColor clearColor];
    }
    if(is_started==1)
    {
        [self.btnJobStart setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        self.viewForTripStart.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblStatus.text=NSLocalizedString(@"YOUR TRIP HAS BEEN STARTED", nil);
        self.lblStatus.textColor=[UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblJobStart.textColor=[UIColor whiteColor];
        self.lblAccept.text=NSLocalizedString(@"IS_STARTED",nil);
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
        self.acceptView.hidden=NO;
        self.statusView.hidden=YES;
    }
    else
    {
        //self.lblJobStart.textColor=[UIColor darkGrayColor];
        [self.btnJobStart setBackgroundImage:[UIImage imageNamed:Nil] forState:UIControlStateNormal];
        self.lblStatus.textColor = [UIColor whiteColor];
        self.viewForTripStart.backgroundColor = [UIColor clearColor];
    }
    if(is_dog_rated==1)
    {
        
    }
    if(is_completed==1)
    {
        [self.btnJobDone setBackgroundImage:[UIImage imageNamed:@"check_box"] forState:UIControlStateNormal];
        self.viewForTripEnd.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        self.lblStatus.textColor=[UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1.0];
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
        self.lblJobDone.textColor=[UIColor whiteColor];
        
        isWalkInStarted=NO;
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        [pref setBool:isWalkInStarted forKey:PREF_IS_WALK_STARTED];
        
    }
    else
    {
        [self.btnJobDone setBackgroundImage:[UIImage imageNamed:Nil] forState:UIControlStateNormal];
        //self.lblJobDone.textColor=[UIColor darkGrayColor];
        self.lblStatus.textColor = [UIColor whiteColor];
        self.viewForTripEnd.backgroundColor = [UIColor clearColor];
    }
    
    if (self.statusView.hidden==NO)
    {
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box_arived"] forState:UIControlStateNormal];
    }
    return 5;
}

#pragma mark -
#pragma mark - Draw Route Methods

-(void)drawPath
{
    NSMutableDictionary *dictPath=[[NSMutableDictionary alloc]init];
    NSString *templati,*templongi;
    
    for (int i=0; i<arrPath.count; i++)
    {
        dictPath=[arrPath objectAtIndex:i];
        templati=[dictPath valueForKey:@"latitude"];
        templongi=[dictPath valueForKey:@"longitude"];
        
        CLLocationCoordinate2D current;
        current.latitude=[templati doubleValue];
        current.longitude=[templongi doubleValue];
        [pathUpdates addLatitude:current.latitude longitude:current.longitude];
        
    }
    
}

-(NSMutableArray *)decodePolyLine: (NSMutableString *)encoded
{
    [encoded replaceOccurrencesOfString:@"\\\\" withString:@"\\" options:NSLiteralSearch range:NSMakeRange(0, [encoded length])];
    NSInteger len = [encoded length];
    NSInteger index = 0;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    NSInteger lat=0;
    NSInteger lng=0;
    while (index < len)
    {
        NSInteger b;
        NSInteger shift = 0;
        NSInteger result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlat = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lat += dlat;
        shift = 0;
        result = 0;
        do
        {
            b = [encoded characterAtIndex:index++] - 63;
            result |= (b & 0x1f) << shift;
            shift += 5;
        } while (b >= 0x20);
        NSInteger dlng = ((result & 1) ? ~(result >> 1) : (result >> 1));
        lng += dlng;
        NSNumber *latitude = [[NSNumber alloc] initWithFloat:lat * 1e-5];
        NSNumber *longitude = [[NSNumber alloc] initWithFloat:lng * 1e-5];
        //printf("[%f,", [latitude doubleValue]);
        //printf("%f]", [longitude doubleValue]);
        CLLocation *loc = [[CLLocation alloc] initWithLatitude:[latitude floatValue] longitude:[longitude floatValue]];
        [array addObject:loc];
    }
    return array;
}

- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}

#pragma mark -
#pragma mark - MKPolyline delegate functions

- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (MKOverlayView *)mapView:(MKMapView *)mapView viewForOverlay:(id <MKOverlay>)overlay
{
    MKPolylineView *polylineView = [[MKPolylineView alloc] initWithPolyline:overlay];
    polylineView.strokeColor = [UIColor colorWithRed:(27.0f/255.0f) green:(151.0f/255.0f) blue:(200.0f/255.0f) alpha:1.0];
    polylineView.lineWidth = 5.0;
    return polylineView;
}
#pragma mark - Tableview Delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(cell == nil)
    {
        cell=[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    NSString *formatedAddress=[[placeMarkArr objectAtIndex:indexPath.row] valueForKey:@"description"]; // AUTOCOMPLETE API
    
    cell.textLabel.text=formatedAddress;
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    aPlacemark=[placeMarkArr objectAtIndex:indexPath.row];
    self.tableForCity.hidden=YES;
    // [self textFieldShouldReturn:nil];
    
    [self setNewPlaceData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return placeMarkArr.count;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(void)setNewPlaceData
{
    self.txtAddress.text = [NSString stringWithFormat:@"%@",[aPlacemark objectForKey:@"description"]];
    [self textFieldShouldReturn:self.txtAddress];
}
#pragma mark
#pragma mark - UITextfield Delegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.txtAddress.text=@"";
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(textField==self.txtAddress)
    {
        if(self.txtAddress.text.length == 0)
        {
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            self.txtAddress.text=[pref valueForKey:PRFE_DESTINATION_ADDRESS];
        }
        else
        {
            [self getLocationFromString:self.txtAddress.text];
        }
    }
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(textField==self.txtPromo)
    {
        [self.txtPromo resignFirstResponder];
    }
    self.tableForCity.hidden=YES;
    [textField resignFirstResponder];
    return YES;
}
-(void)getLocationFromString:(NSString *)str
{
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setObject:str forKey:PARAM_ADDRESS];
    [dictParam setObject:GOOGLE_KEY forKey:PARAM_KEY];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getAddressFromGooglewithParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if(response)
         {
             NSArray *arrAddress=[response valueForKey:@"results"];
             
             if ([arrAddress count] > 0)
                 
             {
                 self.txtAddress.text=[[arrAddress objectAtIndex:0] valueForKey:@"formatted_address"];
                 NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                 [pref setObject:self.txtAddress.text forKey:PRFE_DESTINATION_ADDRESS];
                 NSDictionary *dictLocation=[[[arrAddress objectAtIndex:0] valueForKey:@"geometry"] valueForKey:@"location"];
                 
                 strForDestLat=[dictLocation valueForKey:@"lat"];
                 strForDestLong=[dictLocation valueForKey:@"lng"];
                 CLLocationCoordinate2D coor;
                 coor.latitude=[strForDestLat doubleValue];
                 coor.longitude=[strForDestLong doubleValue];
                 
                 CLLocationCoordinate2D coorStr;
                 coorStr.latitude=[strForLatitude doubleValue];
                 coorStr.longitude=[strForLongitude doubleValue];
                 
                 [self showRouteFrom:coorStr to:coor];
                 
                 [self setDestination];
             }
             
         }
         
     }];
}

-(void)setDestination
{
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc] init];
    [dictParam setValue:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
    [dictParam setValue:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
    [dictParam setValue:[pref objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
    [dictParam setValue:strForDestLat forKey:@"dest_lat"];
    [dictParam setValue:strForDestLong forKey:@"dest_long"];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_SET_DESTINATION withParamData:dictParam withBlock:^(id response, NSError *error)
     {
         if (response)
         {
             NSLog(@"destination response --> %@",response);
         }
     }];
}
-(void)getAddress
{
    NSString *url = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/directions/json?origin=%f,%f&destination=%f,%f&sensor=false",[strForOwnerLat floatValue], [strForOwnerLong floatValue], [strDestinationLat floatValue], [strDestinationLong floatValue]];
    
    NSString *str = [NSString stringWithContentsOfURL:[NSURL URLWithString:url] encoding:NSUTF8StringEncoding error:nil];
    
    NSDictionary *JSON = [NSJSONSerialization JSONObjectWithData: [str dataUsingEncoding:NSUTF8StringEncoding]
                                                         options: NSJSONReadingMutableContainers
                                                           error: nil];
    
    NSDictionary *getRoutes = [JSON valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getDestAddress = [getLegs valueForKey:@"end_address"];
    NSArray *getSourceAddress = [getLegs valueForKey:@"start_address"];
    
    
    if (getDestAddress.count!=0)
    {
        self.lblShowSourceAddress.text = [[getSourceAddress objectAtIndex:0]objectAtIndex:0];
        self.lblShowDestinationAddress.text = [[getDestAddress objectAtIndex:0]objectAtIndex:0];
    }
    
}

#pragma mark-
#pragma mark- Show Route With Google

-(void)showRouteFrom:(CLLocationCoordinate2D)f to:(CLLocationCoordinate2D)t
{
    if(routes)
    {
        [mapView_ clear];
    }
    
//    GMSMarker *markerOwner = [[GMSMarker alloc] init];
//    markerOwner.position = f;
//    markerOwner.map = mapView_;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,70,70)];
    //view.backgroundColor = [UIColor redColor];
    UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"pin_ETA"]];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(13,4, 30, 30)];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.text = strETAmin;
    label.textColor = [UIColor whiteColor];
    label.font = [UberStyleGuide fontRegularBold:12.0f];
    
    UILabel *labelVal = [[UILabel alloc]initWithFrame:CGRectMake(8, 14, 30, 30)];
    labelVal.text = strETAvalue;
    labelVal.textColor = [UIColor whiteColor];
    labelVal.textAlignment = NSTextAlignmentCenter;
    labelVal.font = [UberStyleGuide fontRegularBold:9.0f];
    
    [label sizeToFit];
    [labelVal sizeToFit];
    [view addSubview:pinImageView];
    [view addSubview:label];
    [view addSubview:labelVal];
    
    UIImage *markerIcon = [self imageFromView:view];
    client_marker = [[GMSMarker alloc] init];
    client_marker.position = f;
    client_marker.icon = markerIcon;
    client_marker.map = mapView_;

    /*GMSMarker *markerDriver = [[GMSMarker alloc] init];
    markerDriver.position = f ;
    markerDriver.icon = [UIImage imageNamed:@"pin_client_org"];
    markerDriver.map = mapView_;*/
    
    /*driver_marker = [[GMSMarker alloc] init];
    driver_marker.position = CLLocationCoordinate2DMake([strForWalkStatedLatitude doubleValue], [strForWalkStatedLongitude doubleValue]);
    driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
    driver_marker.map = mapView_;*/
    
    driver_marker = [[GMSMarker alloc] init];
    driver_marker.position = t;
    driver_marker.icon=[UIImage imageNamed:@"pin_driver"];
    driver_marker.map = mapView_;
    
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY_NEW];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] || [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
        
    }
    else
    {
        GMSPath *path =[GMSPath pathFromEncodedPath:json[@"routes"][0][@"overview_polyline"][@"points"]];
        GMSPolyline *singleLine = [GMSPolyline polylineWithPath:path];
        singleLine.strokeWidth = 5.0f;
        singleLine.strokeColor = [UIColor colorWithRed:(0.0f/255.0f) green:(0.0f/255.0f) blue:(0.0f/255.0f) alpha:1.0];
        singleLine.map = mapView_;
        
        routes = json[@"routes"];
        
        NSString *points=[[[routes objectAtIndex:0] objectForKey:@"overview_polyline"] objectForKey:@"points"];
        
        NSArray *temp= [self decodePolyLine:[points mutableCopy]];
        
        [self centerMap:[temp mutableCopy]];
    }
    
    
    
    CLLocationCoordinate2D driverCoorStr;
    driverCoorStr.latitude=[strForWalkStatedLatitude doubleValue];
    driverCoorStr.longitude=[strForWalkStatedLongitude doubleValue];
    
    //[self centerMapFirst:f two:t third:driverCoorStr];
}

-(void)centerMap:(NSArray*)locations
{
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    CLLocationCoordinate2D location;
    for (CLLocation *loc in locations)
    {
        location.latitude = loc.coordinate.latitude;
        location.longitude = loc.coordinate.longitude;
        // Creates a marker in the center of the map.
        bounds = [bounds includingCoordinate:location];
    }
    [mapView_ animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:10.0f]];
}
-(void)centerMapFirst:(CLLocationCoordinate2D)pos1 two:(CLLocationCoordinate2D)pos2 third:(CLLocationCoordinate2D)pos3
{
    GMSCoordinateBounds* bounds =
    [[GMSCoordinateBounds alloc]initWithCoordinate:pos1 coordinate:pos2];
    bounds=[bounds includingCoordinate:pos3];
    CLLocationCoordinate2D location1 = bounds.southWest;
    CLLocationCoordinate2D location2 = bounds.northEast;
    
    float mapViewWidth = mapView_.frame.size.width;
    float mapViewHeight = mapView_.frame.size.height;
    
    MKMapPoint point1 = MKMapPointForCoordinate(location1);
    MKMapPoint point2 = MKMapPointForCoordinate(location2);
    
    MKMapPoint centrePoint = MKMapPointMake(
                                            (point1.x + point2.x) / 2,
                                            (point1.y + point2.y) / 2);
    CLLocationCoordinate2D centreLocation = MKCoordinateForMapPoint(centrePoint);
    
    double mapScaleWidth = mapViewWidth / fabs(point2.x - point1.x);
    double mapScaleHeight = mapViewHeight / fabs(point2.y - point1.y);
    double mapScale = MIN(mapScaleWidth, mapScaleHeight);
    
    double zoomLevel = 19.5 + log2(mapScale);
    
    GMSCameraUpdate *updatedCamera = [GMSCameraUpdate setTarget:centreLocation zoom:zoomLevel];
    [mapView_ animateWithCameraUpdate:updatedCamera];
}

-(void) calculateRoutesFrom:(CLLocationCoordinate2D) f to: (CLLocationCoordinate2D) t
{
    NSString* saddr = [NSString stringWithFormat:@"%f,%f", f.latitude, f.longitude];
    NSString* daddr = [NSString stringWithFormat:@"%f,%f", t.latitude, t.longitude];
    
    NSString* apiUrlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/directions/json?origin=%@&destination=%@&key=%@",saddr,daddr,GOOGLE_KEY_NEW];
    
    NSURL* apiUrl = [NSURL URLWithString:apiUrlStr];
    
    NSError* error = nil;
    NSData *data = [[NSData alloc]initWithContentsOfURL:apiUrl];
    
    NSDictionary *json =[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&error];
    NSDictionary *getRoutes = [json valueForKey:@"routes"];
    NSDictionary *getLegs = [getRoutes valueForKey:@"legs"];
    NSArray *getAddress = [getLegs valueForKey:@"duration"];
    
    if ([[json objectForKey:@"status"]isEqualToString:@"REQUEST_DENIED"] || [[json objectForKey:@"status"] isEqualToString:@"OVER_QUERY_LIMIT"] || [[json objectForKey:@"status"] isEqualToString:@"ZERO_RESULTS"])
    {
        
    }
    else
    {
    
        strETA = [[[getAddress objectAtIndex:0]objectAtIndex:0] valueForKey:@"text"];
        
        NSArray *time=[strETA componentsSeparatedByString:@" "];
        
        strETAmin=[time objectAtIndex:0];
        strETAvalue= [time objectAtIndex:1];
    }
    
}

#pragma  mark-
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSMutableDictionary *dictWalkInfo=[[NSMutableDictionary alloc]init];
    NSString *distance= strDistance;
    
    NSArray *arrDistace=[distance componentsSeparatedByString:@" "];
    float dist;
    dist=[[arrDistace objectAtIndex:0]floatValue];
    if (arrDistace.count>1)
    {
        
        if ([[arrDistace objectAtIndex:1] isEqualToString:@"kms"])
        {
            dist=dist*0.621371;
            
        }
        
    }
    [dictWalkInfo setObject:[NSString stringWithFormat:@"%f",dist] forKey:@"distance"];
    [dictWalkInfo setObject:strTime forKey:@"time"];
    
    if([segue.identifier isEqualToString:SEGUE_TO_FEEDBACK])
    {
        FeedBackVC *obj=[segue destinationViewController];
        obj.dictWalkInfo=dictWalkInfo;
        // obj.dictBillInfo=dictBillInfo;
        obj.strUserImg=strUSerImage;
        obj.strFirstName=self.lblDriverName.text;
        obj.arrSubTypes = arrSubTypes;
    }
}
- (IBAction)contactProviderBtnPressed:(id)sender
{
    NSString *call=[NSString stringWithFormat:@"tel://%@",strProviderPhone];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:call]];
}
- (IBAction)statusBtnPressed:(id)sender
{
    self.acceptView.hidden=YES;
    if (self.statusView.hidden==YES)
    {
        self.statusView.hidden=NO;
        [APPDELEGATE.window addSubview:self.statusView];
        [APPDELEGATE.window bringSubviewToFront:self.statusView];
        //  [APPDELEGATE.window bringSubviewToFront:self.statusView];
    }
    else
    {
        self.statusView.hidden=YES;
        [self.btnStatus setBackgroundImage:[UIImage imageNamed:@"notification_box"] forState:UIControlStateNormal];
        [APPDELEGATE.window bringSubviewToFront:self.statusView];
    }
}

#pragma mark-
#pragma mark- Cash or Card Btn Actions

- (IBAction)onclickExpandBio:(id)sender
{
    UIAlertView *alert =[[UIAlertView alloc] initWithTitle:nil message:self.lblDriverbio.text delegate:self cancelButtonTitle:nil otherButtonTitles:@"CLOSE", nil];
    [alert show];
}

- (IBAction)cardBtnPressed:(id)sender
{
    iscash=NO;
    [self changePaymentType];
}

- (IBAction)cashBtnPressed:(id)sender
{
    iscash=YES;
    [self changePaymentType];
}

-(void)changePaymentType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"PLEASE_WAIT", nil)];
        NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
        
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_ID] forKey:PARAM_ID];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
        [dictParam setValue:[USERDEFAULT objectForKey:PREF_REQ_ID] forKey:PARAM_REQUEST_ID];
        if (iscash)
        {
            [dictParam setValue:@"1" forKey:PARAM_CASH_CARD];
        }
        else
        {
            [dictParam setValue:@"0" forKey:PARAM_CASH_CARD];
        }
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_PAYMENT_TYPE withParamData:dictParam withBlock:^(id response, NSError *error)
         {
             [[AppDelegate sharedAppDelegate]hideLoadingView];
             
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     
                     if(iscash)
                     {
                         [self.btnCard setTitle:NSLocalizedString(@"Card", nil) forState:UIControlStateNormal];
                         [self.btnCard setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                         [self.btnCard setTitle:NSLocalizedString(@"Card", nil) forState:UIControlStateNormal];
                         [self.btnCard setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                         [self.btnCard setBackgroundImage:nil forState:UIControlStateNormal];
                         [self.btnCard setBackgroundColor:[UIColor whiteColor]];
                         [self.btnCash setBackgroundImage:[UIImage imageNamed:@"rectangle2"] forState:UIControlStateNormal];
                         [self.btnCash setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                     }
                     else
                     {
                         [self.btnCard setBackgroundImage:[UIImage imageNamed:@"rectangle2"] forState:UIControlStateNormal];
                         [self.btnCard setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                         [self.btnCash setBackgroundImage:nil forState:UIControlStateNormal];
                         [self.btnCash setBackgroundColor:[UIColor whiteColor]];
                         [self.btnCash setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
                     }
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"SUCCESS", nil)];
                     
                 }
                 else
                 {
                     UIAlertView *alert =[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"ERROR", nil) message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                     [alert show];
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    
}

- (IBAction)cancelBtnPressed:(id)sender
{
    if(is_walker_started==0)
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Cancel Service" message:@"Are You Sure You Want to Cancel Service ?" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
        alert.tag=200;
        [alert show];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Cancel Service" message:@"Are You Sure You Want to Cancel Service ? Cancellation is now chargable" delegate:self cancelButtonTitle:@"CANCEL" otherButtonTitles:@"OK", nil];
        alert.tag=201;
        [alert show];
    }
}

#pragma mark-
#pragma mark- Promo Btn Actions

- (IBAction)promoBtnPressed:(id)sender
{
    self.viewForPromo.hidden=NO;
    self.btnPromoDone.hidden=YES;
    self.viewForPromoMessage.hidden=YES;
}
- (IBAction)promoDoneBtnPressed:(id)sender
{
    self.viewForPromo.hidden=YES;
    self.btnPromoApply.enabled=YES;
    self.btnPromoDone.hidden=YES;
    self.btnPromoCancel.hidden=NO;
    self.txtPromo.text=@"";
}

- (IBAction)promoCancelBtnPressed:(id)sender
{
    self.viewForPromo.hidden=YES;
    self.txtPromo.text=@"";
}

- (IBAction)promoApplyBtnPressed:(id)sender
{
    NSString *promoCode=self.txtPromo.text;
    if(promoCode.length > 0)
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            [dictParam setValue:[pref objectForKey:PREF_USER_ID] forKey:PARAM_ID];
            [dictParam setValue:[pref objectForKey:PREF_USER_TOKEN] forKey:PARAM_TOKEN];
            [dictParam setValue:promoCode forKey:PARAM_PROMO_CODE];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_APPLY_PROMO withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 [[AppDelegate sharedAppDelegate]hideLoadingView];
                 
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         self.viewForPromoMessage.hidden=NO;
                         self.imgForPromoMsg.image=[UIImage imageNamed:@"check_box"];
                         self.lblPromoMsg.textColor=[UIColor colorWithRed:0.0/255.0 green:195.0/255.0 blue:109.0/255.0 alpha:1];
                         self.lblPromoMsg.text=[response valueForKey:@"error"];
                         //self.lblPromoMsg.text=@"your promo code add successfully";
                         self.btnPromoApply.enabled=NO;
                         self.btnPromoCancel.hidden=YES;
                         self.btnPromoDone.hidden=NO;
                     }
                     else
                     {
                         self.viewForPromoMessage.hidden=NO;
                         self.imgForPromoMsg.image=[UIImage imageNamed:@"error"];
                         self.lblPromoMsg.textColor=[UIColor colorWithRed:205.0/255.0 green:0.0/255.0 blue:15.0/255.0 alpha:1];
                         self.lblPromoMsg.text=[response valueForKey:@"error"];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:Nil message:NSLocalizedString(@"Please Promo", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

@end