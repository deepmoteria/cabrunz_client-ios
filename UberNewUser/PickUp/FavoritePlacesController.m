//
//  FavoritePlacesController.m
//  Cabrunz Client
//
//  Created by My Mac on 10/13/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "FavoritePlacesController.h"

@interface FavoritePlacesController ()

@end

@implementation FavoritePlacesController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackBarItem];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onClickClose:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
