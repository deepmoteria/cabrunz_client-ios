//
//  SupportVC.m
//  UberNew
//
//  Created by Elluminati - macbook on 26/09/14.
//  Copyright (c) 2014 Jigs. All rights reserved.
//

#import "HistoryVC.h"
#import "HistoryCell.h"
#import "Constants.h"
#import "UIImageView+Download.h"
#import "AppDelegate.h"
#import "AFNHelper.h"
#import "UtilityClass.h"
#import "UberStyleGuide.h"
#import "SWRevealViewController.h"
#import "subTypeCell.h"
#import "UIView+Utils.h"

@interface HistoryVC ()
{
    NSMutableArray *arrHistory;
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection,*arrSubType;
    NSString *strDistCost;
}

@end

@implementation HistoryVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self SetLocalization];
 
    self.viewForBill.hidden=YES;
    arrHistory=[[NSMutableArray alloc]init];
    //[super setNavBarTitle:TITLE_SUPPORT];
    [super setBackBarItem];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"GETTING HISTORY", nil)];
    [self getHistory];
    [self customSetup];
    
    //self.btnMenu.titleLabel.font = [UberStyleGuide fontRegularNav];
    self.lblDistCost.font=[UberStyleGuide fontRegular];
    self.lblBasePrice.font=[UberStyleGuide fontRegular];
    self.lblPerDist.font=[UberStyleGuide fontRegular];
    self.lblPerTime.font=[UberStyleGuide fontRegular];
    self.lblTimeCost.font=[UberStyleGuide fontRegular];
    self.lblPomoBouns.font=[UberStyleGuide fontRegular];
    self.lblReferralBouns.font=[UberStyleGuide fontRegular];
    //self.lblTotal.font=[UberStyleGuide fontRegularBold:30.0f];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.tableView.hidden=NO;
    self.viewForBill.hidden=YES;
    self.lblnoHistory.hidden=YES;
    self.imgNoDisplay.hidden=YES;
    self.navigationController.navigationBarHidden=NO;
}
- (void)viewDidAppear:(BOOL)animated
{
    [self.btnMenu setTitle:NSLocalizedString(@"NAV_HISTORY", nil) forState:UIControlStateNormal];
}
-(void)SetLocalization
{
    self.lblInvoice.text=NSLocalizedString(@"Invoice", nil);
    self.lTotalCost.text=NSLocalizedString(@"Total Due", nil);
    [self.btnClose setTitle:NSLocalizedString(@"Close",nil) forState:UIControlStateNormal];
}
#pragma mark -
#pragma mark - Memory Mgmt

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark-
#pragma mark - Table view data source

-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrHistory];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO
                                                                            selector:@selector(localizedStandardCompare:)];
    
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
        
    }
    
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView.tag==1)
    {
        return arrForSection.count;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return  [[arrForSection objectAtIndex:section] count];
    }
    else
    {
        return arrSubType.count;
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return 20.0f;
    }
    else
    {
        return 0.0f;
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if(tableView.tag == 1)
    {
        return 1.0;
    }
    else
    {
        return 0.0f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView.tag==1)
    {
    UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
    UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(20, 2, 300, 15)];
    lblDate.font=[UberStyleGuide fontRegular:13.0f];
    lblDate.textColor=[UIColor blackColor];
    NSString *strDate=[arrForDate objectAtIndex:section];
    NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
    UILabel *lblfoot=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, 320,1)];
    lblfoot.backgroundColor=[UIColor blackColor];
    [headerView addSubview:lblfoot];
   
    ///   YesterDay Date Calulation
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:[NSDate date]
                                                  options:0];
    NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
    
    
    if([strDate isEqualToString:current])
    {
        lblDate.text=@"TODAY";
        headerView.backgroundColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];
        lblDate.textColor=[UIColor blackColor];
    }
    else if ([strDate isEqualToString:strYesterday])
    {
        lblDate.text=@"YESTERDAY";
        headerView.backgroundColor=[UIColor grayColor];
        lblDate.textColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];

    }
    else
    {
        NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
        NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];//2nd Jan 2015
        lblDate.text=text;
        headerView.backgroundColor=[UIColor grayColor];
        lblDate.textColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];

    }
    
    [headerView addSubview:lblDate];
    return headerView;
    }
    else
    {
        return nil;
    }
}

/*- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [arrForDate objectAtIndex:section];
}*/

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
   /* if(tableView.tag==1)
    {
        UIImageView *imgFooter=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"rectangle2"]];
        return Footer;
    }
    else
    {
        return nil;
    }*/
    return nil;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
    {

    static NSString *cellIdentifier = @"historycell";
    HistoryCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }

    NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    NSMutableDictionary *dictOwner=[pastDict valueForKey:@"walker"];

    cell.lblName.font=[UberStyleGuide fontRegular:16.0f];
    cell.lblPrice.font=[UberStyleGuide fontRegular:20.0f];
    cell.lblType.font=[UberStyleGuide fontRegular];
    cell.lblTime.font=[UberStyleGuide fontRegular:16.0f];
    //cell.lblTime.textColor = [UIColor blackColor];
    cell.lblName.text=[NSString stringWithFormat:@"%@ %@",[dictOwner valueForKey:@"full_name"],[dictOwner valueForKey:@"last_name"]];
    cell.lblType.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
    cell.lblPrice.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
    
    NSDate *dateTemp=[[UtilityClass sharedObject]stringToDate:[pastDict valueForKey:@"date"]];
    NSString *strDate=[[UtilityClass sharedObject]DateToString:dateTemp withFormate:@"hh:mm a"];
    
    cell.lblTime.text=[NSString stringWithFormat:@"%@",strDate];
        [cell.imageView applyRoundedCornersFullWithColor:[UIColor clearColor]];
    [cell.imageView downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
        return cell;
    }
    else
    {
        static NSString *cellIdentifier = @"subType";
        
        subTypeCell *cell = [self.tblForSubTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[subTypeCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *dict = [arrSubType objectAtIndex:indexPath.row];
        cell.lblSubTypeName.text = [dict valueForKey:@"name"];
        cell.lblSubTypePrise.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
        cell.lblPerMile.hidden = YES;

        /*if(indexPath.row == 1 || indexPath.row == 2)
        {
            cell.lblPerMile.hidden = NO;
        }
        else
        {
            cell.lblPerMile.hidden = YES;
        }
        
        if(indexPath.row == 1)
        {
            cell.lblPerMile.text = [[arrSubType objectAtIndex:1]valueForKey:@"distance_cost_per"];
        }
        if(indexPath.row == 2)
        {
            cell.lblPerMile.text = [[arrSubType objectAtIndex:2]valueForKey:@"time_cost_per"];
        }*/
        
        return cell;

    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
        return 60;
    else
        return 50;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==1)
    {
    
    self.navigationController.navigationBarHidden=YES;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
    NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    self.lblTotal.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
    NSLog(@"Payment Detail:- %@",pastDict);
    arrSubType = [pastDict valueForKey:@"subtype"];
    
        NSArray *arrTemp=[[NSArray alloc]initWithArray:arrSubType];
        arrSubType = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *dictname = [[NSMutableDictionary alloc]init];
        [dictname setObject:NSLocalizedString(@"BASE_PRICE", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"base_price"] forKey:@"price"];
        [arrSubType addObject:dictname];
        
        NSMutableDictionary *dictService = [[NSMutableDictionary alloc]init];
        [dictService setObject:@"Serivce Cost" forKey:@"name"];
        [dictService setObject:[pastDict valueForKey:@"service_cost"] forKey:@"price"];
        [arrSubType addObject:dictService];
        
        /*dictname = [[NSMutableDictionary alloc]init];
        [dictname setObject:NSLocalizedString(@"DISTANCE_COST", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"distance_cost"] forKey:@"price"];
        
        float totalDist=[[pastDict valueForKey:@"distance_cost"] floatValue];
        float Dist=[[pastDict valueForKey:@"distance"]floatValue];
        
        if(Dist!=0)
        {
            strDistCost =[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
        }
        else
        {
            strDistCost=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
        }
        [dictname setObject:strDistCost forKey:@"distance_cost_per"];
        [arrSubType addObject:dictname];

        
        dictname = [[NSMutableDictionary alloc]init];
        [dictname setObject:NSLocalizedString(@"TIME_COST", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"time_cost"] forKey:@"price"];
        
        float totalTime=[[pastDict valueForKey:@"time_cost"] floatValue];
        float Time=[[pastDict valueForKey:@"time"]floatValue];
        if(Time!=0)
        {
            strDistCost=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
        }
        else
        {
            strDistCost=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
        }
        
        [dictname setObject:strDistCost forKey:@"time_cost_per"];
        [arrSubType addObject:dictname];
        [arrSubType addObjectsFromArray:arrTemp];*/
        
        NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
        
        [dictprice setObject:NSLocalizedString(@"PROMO_BONUS", nil) forKey:@"name"];
        [dictprice setObject:[pastDict valueForKey:@"promo_bonus"] forKey:@"price"];
        [arrSubType addObject:dictprice];
        
        NSMutableDictionary *dictReferral = [[NSMutableDictionary alloc]init];
        [dictReferral setObject:NSLocalizedString(@"REFERRAL_BONUS", nil) forKey:@"name"];
        [dictReferral setObject:[pastDict valueForKey:@"referral_bonus"] forKey:@"price"];
        [arrSubType addObject:dictReferral];
        self.tblForSubTypes.tag=2;
        [self.tblForSubTypes reloadData];
        

    [UIView animateWithDuration:0.5 animations:^{
        self.viewForBill.hidden=NO;
    } completion:^(BOOL finished)
     {
     }];
    }
    else
    {
        
    }
    /*
    if(isSubType == YES)
    {
        self.lblBasePrice.text=[NSString stringWithFormat:@"$%@",[pastDict valueForKey:@"base_price"]];
        self.lblDistCost.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"distance_cost"] floatValue]];
        self.lblTimeCost.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"time_cost"] floatValue]];
     self.lblTotal.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
        self.lblPomoBouns.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"promo_bonus"] floatValue]];
        self.lblReferralBouns.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"referral_bonus"] floatValue]];
        float totalDist=[[pastDict valueForKey:@"distance_cost"] floatValue];
        float Dist=[[pastDict valueForKey:@"distance"]floatValue];
        if ([[pastDict valueForKey:@"unit"]isEqualToString:@"kms"])
        {
            totalDist=totalDist*0.621317;
            Dist=Dist*0.621371;
        }
        
        if(Dist!=0)
        {
            self.lblPerDist.text=[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
        }
        else
        {
            self.lblPerDist.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
        }
        
        float totalTime=[[pastDict valueForKey:@"time_cost"] floatValue];
        float Time=[[pastDict valueForKey:@"time"]floatValue];
        if(Time!=0)
        {
            self.lblPerTime.text=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
        }
        else
        {
            self.lblPerTime.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
        }
        
        

    }
    else
    {
        
    }
    */
    
    
    
}
#pragma mark -
#pragma mark - Custom Methods
- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.btnMenu addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        [self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}

-(void)getHistory
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
       NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
       NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_HISTORY,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"History Data= %@",response);
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     [APPDELEGATE hideLoadingView];
                     
                     arrHistory=[response valueForKey:@"requests"];
                     NSLog(@"History count = %lu",(unsigned long)arrHistory.count);
                     if (arrHistory.count==0 || arrHistory==nil)
                     {
                         self.tableView.hidden=YES;
                         self.lblnoHistory.hidden=NO;
                         self.imgNoDisplay.hidden=NO;
                     }
                     else
                     {
                         self.tableView.hidden=NO;
                         self.lblnoHistory.hidden=YES;
                         self.imgNoDisplay.hidden=YES;
                         [self makeSection];
                         [self.tableView reloadData];
                         
                     }
                     
                 }
             }
             
         }];
        
        
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"No Internet" message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}
        
- (IBAction)closeBtnPressed:(id)sender
{
    self.navigationController.navigationBarHidden=NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.viewForBill.hidden=YES;
    } completion:^(BOOL finished)
     {
     }];
}

@end
