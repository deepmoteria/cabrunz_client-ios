//
//  StripePaymentVC.h
//  Dapper
//
//  Created by My Mac on 4/20/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardIO.h"
#import "PTKView.h"
#import "Stripe.h"
#import "AppDelegate.h"
#import "Constants.h"
#import "AFNHelper.h"
#import "PTKTextField.h"
#import "BaseVC.h"

#define Card_Length 16
#define Card_Month 2
#define Card_Year 2
#define Card_CVC_CVV 3

@interface StripePaymentVC : BaseVC <CardIOPaymentViewControllerDelegate,PTKViewDelegate,UITextFieldDelegate>
{
    NSString *strForID;
    NSString *strForToken;
    NSString *strForStripeToken,*strForLastFour;
}

@property (weak, nonatomic) IBOutlet PTKView *paymentView;

- (void)createBackendChargeWithToken:(STPToken *)token;
- (IBAction)addCardBtnPressed:(id)sender;
- (IBAction)scanBtnPress:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *imgNoItems;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;

@property (weak, nonatomic) IBOutlet UITextField *txtMonth;
@property (weak, nonatomic) IBOutlet UITextField *txtYear;
@property (weak, nonatomic) IBOutlet UITextField *txtCVC;
@property (weak, nonatomic) IBOutlet UITextField *txtBnak;




@end