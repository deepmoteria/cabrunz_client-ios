//
//  SelectPropsCellVC.h
//  TaxiNow
//
//  Created by My Mac on 6/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectPropsCellVC : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblProps;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelectedCell;

@end
