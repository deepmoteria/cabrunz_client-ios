//
//  SelectPropsVC.h
//  TaxiNow
//
//  Created by My Mac on 6/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"
#import "RateView.h"
#import "RatingBar.h"

@interface SelectPropsVC : BaseVC<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
{
    NSString *strForUserNote, *strForUserId, *strForUserToken,*strForRequestID, *strForDriverLatitude, *strForDriverLongitude;
    NSTimer *timerForCheckReqStatus;
    NSMutableDictionary *driverInfo;
}

// pick up vc

@property (weak, nonatomic) IBOutlet UIView *viewForTakePhoto;
@property (weak, nonatomic) IBOutlet UIView *viewForNote;
@property (weak, nonatomic) IBOutlet UIImageView *imgForRequest;
@property (weak, nonatomic) IBOutlet UIButton *btnSkipStep;
@property (weak, nonatomic) IBOutlet UIButton *btnSubmitStep;
@property (weak, nonatomic) IBOutlet UITextView *txtNote;


- (IBAction)onClickBackPhoto:(id)sender;

- (IBAction)onClickClosePhoto:(id)sender;

- (IBAction)onClickSkipBtnStep:(id)sender;

- (IBAction)onclickSubmitBtnStep:(id)sender;

- (IBAction)onclickTakePhoto:(id)sender;



@property (strong, nonatomic) UIImage *img;
@property (weak, nonatomic) IBOutlet UITableView *tblForProps;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectProps;
@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property(weak,nonatomic)NSString *strGetSourceAddress;
@property(weak,nonatomic)NSString *strGetDestinationAddress;
@property(weak,nonatomic)NSString *strForLatitude;
@property(weak,nonatomic)NSString *strForLongitude;
@property(weak,nonatomic)NSString *strForDestLatitude;
@property(weak,nonatomic)NSString *strForDestLongitude;
@property(weak,nonatomic)NSString *strForTypeId,*strNote;
@property(weak,nonatomic) UIImageView *imgRequest;

- (IBAction)cancelReqBtnPressed:(id)sender;
- (IBAction)selectPropsBtnPressed:(id)sender;


////profile view
@property (weak, nonatomic) IBOutlet UIView *viewForProviderInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@property (weak, nonatomic) IBOutlet UIImageView *imgProfilePic;

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet RatingBar *ratingView;
@end