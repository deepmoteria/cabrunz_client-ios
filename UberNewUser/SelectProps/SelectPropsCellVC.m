//
//  SelectPropsCellVC.m
//  TaxiNow
//
//  Created by My Mac on 6/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "SelectPropsCellVC.h"
#import "UIView+Utils.h"

@implementation SelectPropsCellVC

- (void)awakeFromNib {
    // Initialization code
    [self.imgSelectedCell applyRoundedCornersFull];
    self.lblProps.font=[UberStyleGuide fontRegularLightBig];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
