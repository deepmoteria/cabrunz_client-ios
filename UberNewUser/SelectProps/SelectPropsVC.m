//
//  SelectPropsVC.m
//  TaxiNow
//
//  Created by My Mac on 6/2/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "SelectPropsVC.h"
#import "SelectPropsCellVC.h"
#import "SWRevealViewController.h"
#import "WriteNoteVC.h"
#import "ProviderDetailsVC.h"
#import "UIImageView+Download.h"
#import "RateView.h"
#import "RatingBar.h"
#import "UIView+Utils.h"

@interface SelectPropsVC ()
{
    NSMutableArray *arrForSelectProps ,*arrForIndex, *arrForSelected,*arrForTypeName,*arrForSubTypeId;
    NSString *strAvailId,*strForeNote;
    UIImageView *requestImg;
    BOOL isPhoto;
}
@end

@implementation SelectPropsVC

#pragma mark -
#pragma mark - Init

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}
#pragma mark -
#pragma mark - ViewLife Cycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.viewForTakePhoto.hidden=YES;
    self.viewForNote.hidden=YES;
    self.btnSubmitStep.hidden=YES;
    self.btnSkipStep.hidden=YES;
    isPhoto=0;
    
    [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor whiteColor]];
    [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
    strForeNote = self.strNote;
    /*if(self.imgRequest.image==nil)
        requestImg.image = [UIImage imageNamed:@""];
    else*/
    requestImg.image = self.img;
    [[NSUserDefaults standardUserDefaults]setBool:YES forKey:@"isClear"];

    //[super setNavBarTitle:NSLocalizedString(@"STEP 2 OF 3", nil)];
    [self.btnSelectProps setTitle:NSLocalizedString(@"SELECT_PROPS_YOU_WANT", nil) forState:UIControlStateNormal];
    [self.btnSelectProps setTitle:NSLocalizedString(@"SELECT_PROPS_YOU_WANT", nil) forState:UIControlStateHighlighted];
    self.btnCancel.hidden = YES;
    self.viewForProviderInfo.hidden=YES;
    self.btnSelectProps=[APPDELEGATE setBoldFontDiscriptor:self.btnSelectProps];
    [super setBackBarItem];
    [self customSetup];
    arrForSelectProps = [[NSMutableArray alloc]init];
    [self getSubType];
    arrForIndex = [[NSMutableArray alloc]init];
    arrForSelected = [[NSMutableArray alloc]init];
    arrForSubTypeId = [[NSMutableArray alloc]init];
    self.revealButtonItem.titleLabel.font = [UberStyleGuide fontRegularNav];
    UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(Keyboard:)];
    tapGesture.cancelsTouchesInView = NO;
    tapGesture.delegate=self;
    [tapGesture setNumberOfTapsRequired:1];
    [tapGesture setNumberOfTouchesRequired:1];
    [self.viewForNote addGestureRecognizer:tapGesture];
    [self.view addGestureRecognizer:tapGesture];
    
    self.lblName.textColor = [UIColor whiteColor];
    self.lblTitle.textColor = [UIColor whiteColor];
    [self.ratingView initRateBar];
    [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1.0]];
}

- (void)Keyboard:(UIGestureRecognizer *)gst
{
    [self.txtNote resignFirstResponder];
}

#pragma mark-
#pragma mark - custom ws method

-(void)getSubType
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:FILE_APPLICATION_TYPE withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue])
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [arrForSelectProps addObjectsFromArray:[response valueForKey:@"types"]];
                     [self.tblForProps reloadData];
                }
             }
             
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (void)customSetup
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        [self.revealButtonItem addTarget:self.revealViewController action:@selector(revealToggle:) forControlEvents:UIControlEventTouchUpInside];
        //[self.navigationController.navigationBar addGestureRecognizer:revealViewController.panGestureRecognizer];
    }
}
#pragma mark-
#pragma mark - Table view data source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrForSelectProps.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SelectPropsCell";
    
    SelectPropsCellVC *cell = [self.tblForProps dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    
    if (cell==nil)
    {
        cell=[[SelectPropsCellVC alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    NSDictionary *dict = [arrForSelectProps objectAtIndex:indexPath.row];
    cell.lblProps.text = [dict valueForKey:@"name"];
    cell.lblProps.textColor = [UIColor grayColor];
    
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}

#pragma mark-
#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //NSString *name = [[arrForSelectProps objectAtIndex:indexPath.row] valueForKey:@"name"];
   // NSString *subTypeId = [[arrForSelectProps objectAtIndex:indexPath.row] valueForKey:@"id"];
    SelectPropsCellVC *Cell = (SelectPropsCellVC*)[self.tblForProps cellForRowAtIndexPath:indexPath];
    
    if([Cell.imgSelectedCell.image isEqual:[UIImage imageNamed:@"btn_uncheck"]])
    {
        NSDictionary *dictType=[arrForSelectProps objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        [arrForSubTypeId addObject:strTypeId];
        //[arrForSelected addObject:name];
        Cell.imgSelectedCell.image=[UIImage imageNamed:@"btn_check"];
        Cell.lblProps.textColor = [UIColor whiteColor];
        Cell.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1];
    }
    else
    {
        Cell.imgSelectedCell.image=[UIImage imageNamed:@"btn_uncheck"];
        Cell.lblProps.textColor = [UIColor grayColor];
        Cell.backgroundColor = [UIColor clearColor];
    }
    
    /*if([self isRowSelectedOnTableView:tableView atIndexPath:indexPath]){
        [arrForIndex removeObject:indexPath];
        [arrForSelected removeObject:name];
        [arrForSubTypeId removeObject:subTypeId];
        NSLog(@" remov element%@",arrForSelected);
        cell.imgSelectedCell.hidden = YES;
        cell.lblProps.font=[UberStyleGuide fontRegularLightBig];
        [cell setBackgroundColor:[UIColor whiteColor]];
    } else {
        [arrForIndex addObject:indexPath];
        [arrForSelected addObject:name];
        [arrForSubTypeId addObject:subTypeId];
        
        NSLog(@"added elememt = %@",arrForSelected);
        cell.imgSelectedCell.hidden = NO;
        [cell setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:241.0f/255.0f alpha:1]];
        cell.lblProps.font = [UberStyleGuide fontRegularLightBigBold];
        
    }*/
    
    [self.revealViewController rightRevealToggle:nil];

    [self.tblForProps deselectRowAtIndexPath:indexPath animated:YES];
    NSLog(@"%@",arrForIndex);
    
}
 /*-(BOOL)isRowSelectedOnTableView:(UITableView *)tableView atIndexPath:(NSIndexPath *)indexPath
 {
 return ([arrForIndex containsObject:indexPath]) ? YES : NO;
 }*/


#pragma mark-
#pragma mark- Image Controller methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSLog(@"Media Info: %@", info);
    NSString *mediaType = [info valueForKey:UIImagePickerControllerMediaType];
    
    UIImage *photoTaken = [info objectForKey:@"UIImagePickerControllerEditedImage"];
    
    /*NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
     [pref setObject:photoTaken forKey:PARAM_DEVICE_PICTURE];
     [pref synchronize];*/
    self.imgForRequest.image=photoTaken;
    
    isPhoto=1;
    
    if(isPhoto==1)
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }
    else
    {
        self.viewForTakePhoto.hidden=YES;
        self.viewForNote.hidden=NO;
        self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];
        self.btnSubmitStep.hidden=NO;
        self.btnSkipStep.hidden=YES;
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
    UIAlertView *alert;
    
    if (error) {
        alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                           message:[error localizedDescription]
                                          delegate:nil
                                 cancelButtonTitle:@"OK"
                                 otherButtonTitles:nil];
        [alert show];
    }
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissModalViewControllerAnimated:YES];
}

#pragma mark-
#pragma mark - Action Method

- (IBAction)onclickTakePhoto:(id)sender
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
        imagePicker.delegate = self;
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.allowsEditing = YES;
        
        [self presentModalViewController:imagePicker animated:YES];
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera Unavailable"
                                                       message:@"Unable to find a camera on your device."
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
    
    /*if(isPhoto==1)
     {
     self.viewForTakePhoto.hidden=YES;
     self.viewForNote.hidden=NO;
     self.btnSubmitStep.hidden=NO;
     self.btnSkipStep.hidden=YES;
     }
     else
     {
     self.viewForTakePhoto.hidden=YES;
     self.viewForNote.hidden=NO;
     self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];
     self.btnSubmitStep.hidden=NO;
     self.btnSkipStep.hidden=YES;
     }*/
}

/*- (IBAction)cancelBtnPressed:(id)sender
{
    //[self.paymentView setHidden:YES];
    self.tblForProps.hidden = NO;
    [self.tblForProps reloadData];
    self.btnSelectProps.hidden = NO;
}*/
- (IBAction)onClickBackPhoto:(id)sender {
    
    self.viewForNote.hidden=YES;
    self.viewForTakePhoto.hidden=NO;
    self.btnSubmitStep.hidden=YES;
    self.btnSkipStep.hidden=NO;
    self.txtNote.text=@"";
    self.imgForRequest.image=[UIImage imageNamed:@"no_photo"];
}

- (IBAction)onClickClosePhoto:(id)sender
{
    self.view.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1];
    self.tblForProps.hidden=NO;
    [self.tblForProps reloadData];
    self.btnSelectProps.hidden=NO;
    self.viewForTakePhoto.hidden=YES;
    self.btnSkipStep.hidden = YES;
    self.viewForNote.hidden=YES;
    self.btnSubmitStep.hidden = YES;
}

- (IBAction)onClickSkipBtnStep:(id)sender
{
    self.viewForTakePhoto.hidden=YES;
    self.viewForNote.hidden=NO;
    self.btnSkipStep.hidden=YES;
    self.btnSubmitStep.hidden=NO;
    self.txtNote.text=@"";
    
    /*if(self.imgForRequest.image==nil)
     self.imgForRequest.image = [UIImage imageNamed:@"no_photo"];*/
}

- (IBAction)onclickSubmitBtnStep:(id)sender
{
    if([self.txtNote.text isEqualToString:@""])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please Enter Note First" delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
    else
    {
        self.viewForNote.hidden=YES;
        self.btnSkipStep.hidden=YES;
        self.btnSubmitStep.hidden=YES;
        self.viewForTakePhoto.hidden=YES;
    
    // request
    
    if([APPDELEGATE connected])
    {
        strAvailId = @"";
        for (int i=0; i<arrForSubTypeId.count; i++)
        {
            NSString *strTemp;
            if (i!=(arrForSubTypeId.count-1))
            {
                strTemp=[NSString stringWithFormat:@"%@,",[arrForSubTypeId objectAtIndex:i]];
            }
            else
            {
                strTemp=[NSString stringWithFormat:@"%@",[arrForSubTypeId objectAtIndex:i]];
            }
            strAvailId = [strAvailId stringByAppendingString:strTemp];
        }
        
        if ([self.strForTypeId isEqualToString:@"0"]||self.strForTypeId==nil)
            self.strForTypeId=@"1";
        
        if(![self.strForTypeId isEqualToString:@"0"])
        {
            if([[AppDelegate sharedAppDelegate]connected])
            {
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING_WIZARDS", nil)];
                NSTimeZone *timeZone = [NSTimeZone localTimeZone];
                NSString *tzName = [timeZone name];
                self.view.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1];
                NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                strForUserId=[pref objectForKey:PREF_USER_ID];
                strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                
                NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                [dictParam setValue:self.strForLatitude forKey:PARAM_LATITUDE];
                [dictParam setValue:self.strForLongitude  forKey:PARAM_LONGITUDE];
                //[dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                [dictParam setValue:strForUserId forKey:PARAM_ID];
                [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                [dictParam setValue:strAvailId forKey:PARAM_TYPE];
                //[dictParam setValue:@"1" forKey:PARAM_PAYMENT_OPT];
                [dictParam setValue:self.txtNote.text forKey:@"note"];
                [dictParam setValue:tzName forKey:PARAM_TIMEZONE];
                //[dictParam setValue:strAvailId forKey:PARAM_SUB_TYPE];
                //[dictParam setValue:requestImg.image forKey:@"picture"];
                
                //UIImageView *imgUpload = [pref objectForKey:PARAM_DEVICE_PICTURE];
                
               // if(![self.imgForRequest.image isEqual:nil])
                if (isPhoto==1)
                {
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamDataImage:dictParam andImage:self.imgForRequest.image withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             
                             if ([response isKindOfClass:[NSNull class]])                             {
                                 NSLog(@"hello");
                                 [APPDELEGATE hideLoadingView];
                             }
                             else
                             {
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSLog(@"pick up......%@",response);
                                     if([[response valueForKey:@"success"]boolValue])
                                     {
                                         NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                         
                                         [self.ratingView setUserInteractionEnabled:NO];
                                         RBRatings rate=([[walker valueForKey:@"rating"]floatValue]*2);
                                         [self.ratingView setRatings:rate];
                                         
                                         [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor whiteColor]];
                                         [self.imgProfilePic downloadFromURL:[NSString stringWithFormat:@"%@",[walker valueForKey:@"picture"]] withPlaceholder:nil];
                                         
                                         self.lblName.text = [NSString stringWithFormat:@"%@",[walker valueForKey:@"full_name"]];
                                         
                                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                         
                                         strForRequestID=[response valueForKey:@"request_id"];
                                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                         [self setTimerToCheckDriverStatus];
                                         
                                         [self.viewForProviderInfo setHidden:NO];
                                         [self.btnCancel setHidden:NO];
                                         self.lblTitle.text = NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil);
                                         [APPDELEGATE.window addSubview:self.viewForProviderInfo];
                                         [APPDELEGATE.window bringSubviewToFront:self.viewForProviderInfo];
                                         /*
                                          [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                          [self.btnCancel setHidden:NO];
                                          [APPDELEGATE.window addSubview:self.btnCancel];
                                          [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                          */
                                     }
                                     
                                 }
                                 else
                                 {
                                     if([[response valueForKey:@"error_code"] intValue]==406)
                                         [APPDELEGATE checkLogIn];
                                     
                                 }
                             }
                         }
                         
                     }];
                }
                else
                {
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             if ([response isKindOfClass:[NSNull class]])                             {
                                 NSLog(@"hello");
                                 [APPDELEGATE hideLoadingView];
                             }
                             else
                             {
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSLog(@"pick up......%@",response);
                                     if([[response valueForKey:@"success"]boolValue])
                                     {
                                         NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                         
                                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                         
                                         
                                         [self.ratingView setUserInteractionEnabled:NO];
                                         RBRatings rate=([[walker valueForKey:@"rating"]floatValue]*2);
                                         [self.ratingView setRatings:rate];
                                         
                                         [self.imgProfilePic applyRoundedCornersFullWithColor:[UIColor whiteColor]];
                                         [self.imgProfilePic downloadFromURL:[NSString stringWithFormat:@"%@",[walker valueForKey:@"picture"]] withPlaceholder:nil];
                                         
                                         self.lblName.text = [NSString stringWithFormat:@"%@",[walker valueForKey:@"full_name"]];
                                         
                                         strForRequestID=[response valueForKey:@"request_id"];
                                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                         
                                         [self setTimerToCheckDriverStatus];
                                         
                                         
                                         [self.viewForProviderInfo setHidden:NO];
                                         [self.btnCancel setHidden:NO];
                                         self.lblTitle.text = NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil);
                                         [APPDELEGATE.window addSubview:self.viewForProviderInfo];
                                         [APPDELEGATE.window bringSubviewToFront:self.viewForProviderInfo];
                                         
                                         /*
                                          [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                          [self.btnCancel setHidden:NO];
                                          [APPDELEGATE.window addSubview:self.btnCancel];
                                          [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                          */
                                         
                                     }
                                 }
                                 else
                                 {
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                     alert.tag=300;
                                     [alert show];
                                     
                                 }
                             }
                         }
                         
                     }];
                    
                }
                
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                [alert show];
            }
        }
        
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> uHoo -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
}
    
    //[self performSegueWithIdentifier:SEGUE_TO_WRITE_NOTE sender:nil];
}

- (IBAction)selectPropsBtnPressed:(id)sender
{
    self.view.backgroundColor = [UIColor whiteColor];
    self.viewForTakePhoto.hidden=NO;
    self.btnSubmitStep.hidden=YES;
    self.btnSkipStep.hidden=NO;
    self.btnSelectProps.hidden=YES;
    self.tblForProps.hidden=YES;
}
    
    /*if([APPDELEGATE connected])
    {
    strAvailId = @"";
    for (int i=0; i<arrForSubTypeId.count; i++)
       {
        NSString *strTemp;
        if (i!=(arrForSubTypeId.count-1))
        {
            strTemp=[NSString stringWithFormat:@"%@,",[arrForSubTypeId objectAtIndex:i]];
        }
        else
        {
            strTemp=[NSString stringWithFormat:@"%@",[arrForSubTypeId objectAtIndex:i]];
        }
        strAvailId = [strAvailId stringByAppendingString:strTemp];
        }

        if ([self.strForTypeId isEqualToString:@"0"]||self.strForTypeId==nil)
            self.strForTypeId=@"1";
    
        if(![self.strForTypeId isEqualToString:@"0"])
        {
            if([[AppDelegate sharedAppDelegate]connected])
            {
                [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"REQUESTING", nil)];
                    
                    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                    strForUserId=[pref objectForKey:PREF_USER_ID];
                    strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
                    
                    NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
                    [dictParam setValue:self.strForLatitude forKey:PARAM_LATITUDE];
                    [dictParam setValue:self.strForLongitude  forKey:PARAM_LONGITUDE];
                    //[dictParam setValue:@"1" forKey:PARAM_DISTANCE];
                    [dictParam setValue:strForUserId forKey:PARAM_ID];
                    [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
                    [dictParam setValue:strAvailId forKey:PARAM_TYPE];
                    //[dictParam setValue:@"1" forKey:PARAM_PAYMENT_OPT];
                    [dictParam setValue:strForeNote forKey:@"note"];
                    //[dictParam setValue:strAvailId forKey:PARAM_SUB_TYPE];
                    //[dictParam setValue:requestImg.image forKey:@"picture"];
                
                    //UIImageView *imgUpload = [pref objectForKey:PARAM_DEVICE_PICTURE];
                
                    if(![self.img isEqual:nil])
                    {
                
                    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                    [afn getDataFromPath:FILE_CREATE_REQUEST withParamDataImage:dictParam andImage:self.img withBlock:^(id response, NSError *error)
                     {
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         if (response)
                         {
                             if([[response valueForKey:@"success"]boolValue])
                             {
                                 NSLog(@"pick up......%@",response);
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                     
                                     strForRequestID=[response valueForKey:@"request_id"];
                                     [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                     [self setTimerToCheckDriverStatus];
                                     [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                     [self.btnCancel setHidden:NO];
                                     [APPDELEGATE.window addSubview:self.btnCancel];
                                     [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                     
                                 }
                             }
                             else
                             {
                                 if([[response valueForKey:@"error_code"] intValue]==406)
                                     [APPDELEGATE checkLogIn];             
                             
                             }
                         }
                         
                         
                     }];
                    }
                    else
                    {
                        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
                        [afn getDataFromPath:FILE_CREATE_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
                        {
                             [[AppDelegate sharedAppDelegate]hideLoadingView];
                             if (response)
                             {
                                 if([[response valueForKey:@"success"]boolValue])
                                 {
                                     NSLog(@"pick up......%@",response);
                                     if([[response valueForKey:@"success"]boolValue])
                                     {
                                         NSMutableDictionary *walker=[response valueForKey:@"walker"];
                                         NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                                         
                                         strForRequestID=[response valueForKey:@"request_id"];
                                         [pref setObject:strForRequestID forKey:PREF_REQ_ID];
                                         [self setTimerToCheckDriverStatus];
                                         [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"COTACCTING_SERVICE_PROVIDER", nil)];
                                         [self.btnCancel setHidden:NO];
                                         [APPDELEGATE.window addSubview:self.btnCancel];
                                         [APPDELEGATE.window bringSubviewToFront:self.btnCancel];
                                         
                                     }
                                 }
                                 else
                                 {
                                     UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                                     [alert show];
                                     
                                 }
                             }
                             
                             
                         }];
                    
                }
                
                }
                else
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
                    [alert show];
                }
            }
    
        else
            [APPDELEGATE showToastMessage:NSLocalizedString(@"SELECT_TYPE", nil)];
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> uHoo -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }

    //[self performSegueWithIdentifier:SEGUE_TO_WRITE_NOTE sender:nil];
}*/


#pragma mark -
#pragma mark - Navigation


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==300)
    {
        self.tblForProps.hidden=NO;
        self.btnSelectProps.hidden=NO;
        [self.tblForProps reloadData];
    }
}

-(void)setTimerToCheckDriverStatus
{
    if (timerForCheckReqStatus)
    {
        [timerForCheckReqStatus invalidate];
        timerForCheckReqStatus = nil;
    }
    
    timerForCheckReqStatus = [NSTimer scheduledTimerWithTimeInterval:10.0 target:self selector:@selector(checkForRequestStatusInfo) userInfo:nil repeats:YES];
}

-(void)checkForRequestStatusInfo
{
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        strForUserId=[pref objectForKey:PREF_USER_ID];
        strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
        
        NSString *strForUrl=[NSString stringWithFormat:@"%@?%@=%@&%@=%@&%@=%@",FILE_GET_REQUEST,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken,PARAM_REQUEST_ID,strReqId];
        
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:strForUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             if (response)
             {
                 if([[response valueForKey:@"success"]boolValue] && [[response valueForKey:@"confirmed_walker"] integerValue]!=0)
                 {
                     NSLog(@"GET REQ--->%@",response);
                     NSString *strCheck=[response valueForKey:@"walker"];
                     
                     if(strCheck)
                     {
                         // self.btnCancel.hidden=YES;
                         // self.viewForDriver.hidden=YES;
                         //[self.btnCancel removeFromSuperview];
                         
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         NSMutableDictionary *dictWalker=[response valueForKey:@"walker"];
                         strForDriverLatitude=[dictWalker valueForKey:@"latitude"];
                         strForDriverLongitude=[dictWalker valueForKey:@"longitude"];
                         if ([[response valueForKey:@"is_walker_rated"]integerValue]==1)
                         {
                             [pref removeObjectForKey:PREF_REQ_ID];
                             return ;
                         }
                         
                         ProviderDetailsVC *vcFeed = nil;
                         for (int i=0; i<self.navigationController.viewControllers.count; i++)
                         {
                             UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
                             if ([vc isKindOfClass:[ProviderDetailsVC class]])
                             {
                                 vcFeed = (ProviderDetailsVC *)vc;
                             }
                             
                         }
                         if (vcFeed==nil)
                         {
                             [timerForCheckReqStatus invalidate];
                             timerForCheckReqStatus=nil;
                             [self.btnCancel setHidden:YES];
                             [self.viewForProviderInfo setHidden:YES];
                             [self performSegueWithIdentifier:SEGUE_TO_TRIP sender:self];
                         }
                         else
                         {
                             [self.navigationController popToViewController:vcFeed animated:NO];
                         }
                     }
                     
                 }
                 if([[response valueForKey:@"confirmed_walker"] intValue]==0 && [[response valueForKey:@"status"] intValue]==1)
                 {
                     [[AppDelegate sharedAppDelegate]hideLoadingView];
                     [timerForCheckReqStatus invalidate];
                     timerForCheckReqStatus=nil;
                     NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
                     [pref removeObjectForKey:PREF_REQ_ID];
                     
                     [APPDELEGATE showToastMessage:NSLocalizedString(@"NO_WALKER", nil)];
                     [APPDELEGATE hideLoadingView];
                     self.btnCancel.hidden=YES;
                     self.viewForProviderInfo.hidden=YES;
                     self.view.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1];
                     self.tblForProps.hidden = NO;
                     [self.tblForProps reloadData];
                     self.btnSelectProps.hidden = NO;
                     
                     // [self.btnCancel removeFromSuperview];
                     // [self showMapCurrentLocatinn];
                 }
                 else
                 {
                     driverInfo=[response valueForKey:@"walker"];
                 }
             }
             
             else
             {}
         }];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
        [alert show];
    }
}

- (IBAction)cancelReqBtnPressed:(id)sender
{
    if([CLLocationManager locationServicesEnabled])
    {
        if([[AppDelegate sharedAppDelegate]connected])
        {
            [[AppDelegate sharedAppDelegate]hideLoadingView];
            [[AppDelegate sharedAppDelegate]showLoadingWithTitle:NSLocalizedString(@"CANCLEING", nil)];
            
            NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
            strForUserId=[pref objectForKey:PREF_USER_ID];
            strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
            NSString *strReqId=[pref objectForKey:PREF_REQ_ID];
            
            NSMutableDictionary *dictParam=[[NSMutableDictionary alloc]init];
            
            [dictParam setValue:strForUserId forKey:PARAM_ID];
            [dictParam setValue:strForUserToken forKey:PARAM_TOKEN];
            [dictParam setValue:strReqId forKey:PARAM_REQUEST_ID];
            
            AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
            [afn getDataFromPath:FILE_CANCEL_REQUEST withParamData:dictParam withBlock:^(id response, NSError *error)
             {
                 if (response)
                 {
                     if([[response valueForKey:@"success"]boolValue])
                     {
                         [timerForCheckReqStatus invalidate];
                         timerForCheckReqStatus=nil;
                         [[AppDelegate sharedAppDelegate]hideLoadingView];
                         self.btnCancel.hidden=YES;
                         self.viewForProviderInfo.hidden=YES;
                         //[self.btnCancel removeFromSuperview];
                         [APPDELEGATE showToastMessage:NSLocalizedString(@"REQUEST_CANCEL", nil)];
                         self.tblForProps.hidden = NO;
                         [self.tblForProps reloadData];
                         self.btnSelectProps.hidden = NO;
                         self.txtNote.text = @"";
                         
                     }
                     else
                     {
                         if([[response valueForKey:@"error_code"] intValue]==406)
                             [APPDELEGATE checkLogIn];
                     }
                 }
                 
                 
             }];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Network Status", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:nil otherButtonTitles:NSLocalizedString(@"OK", nil), nil];
            [alert show];
        }
        
    }
    else
    {
        UIAlertView *alertLocation=[[UIAlertView alloc]initWithTitle:@"" message:@"Please Enable location access from Setting -> uHoo -> Privacy -> Location services" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alertLocation.tag=100;
        [alertLocation show];
        
    }
    
}

/*- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    WriteNoteVC *obj = [segue destinationViewController];
    obj.strGetSourceAddress = self.strGetSourceAddress;
    obj.strGetDestinationAddress = self.strGetDestinationAddress;
    obj.strForLatitude = self.strForLatitude;
    obj.strForLongitude =self.strForLongitude;
    //obj.strForDestLatitude = self.strForDestLatitude;
    //obj.strForDestLongitude = self.strForDestLongitude;
    obj.arrForProps = arrForSelected;
    //obj.strAvailId = strAvailId;
    //obj.strForTypeId = self.strForTypeId;
}*/

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark
#pragma mark - UITextView Delegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,-112,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
    return YES;
}

-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,-112,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
}

-(void)textViewDidEndEditing:(UITextView *)textView
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.25];
    self.viewForNote.frame = CGRectMake(18,76,self.viewForNote.bounds.size.width,self.viewForNote.bounds.size.height);
    [UIView commitAnimations];
    [self.view resignFirstResponder];
}

@end
