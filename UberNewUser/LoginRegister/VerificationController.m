//
//  VerificationController.m
//  Cabrunz Client
//
//  Created by My Mac on 10/9/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "VerificationController.h"
#import "AppDelegate.h"
#import "UberStyleGuide.h"
#import "AFNetworking.h"
#import "AFNHelper.h"

@interface VerificationController ()

@end

@implementation VerificationController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setBackBarItem];
    [self.txtCode setValue:[UIColor lightGrayColor] forKeyPath:@"_placeholderLabel.textColor"];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.btnVerify setTitle:NSLocalizedString(@"VERIFY", nil) forState:UIControlStateNormal];
    [self.btnResendOtp setTitle:NSLocalizedString(@"RESEND_OTP", nil) forState:UIControlStateNormal];
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGestureVerification:)];
    singleTapGestureRecognizer.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:singleTapGestureRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)handleSingleTapGestureVerification:(UITapGestureRecognizer *)tapGestureRecognizer;
{
    [self.txtCode resignFirstResponder];
}

- (IBAction)onClickVerify:(id)sender {
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        NSString *strId = [pref objectForKey:PREF_USER_ID];
        NSString *strToken = [pref objectForKey:PREF_USER_TOKEN];
        NSString *strOtpCode = self.txtCode.text;
        
        NSMutableDictionary *dictOtpParam = [[NSMutableDictionary alloc]init];
        
        [dictOtpParam setValue:strId forKey:@"id"];
        [dictOtpParam setValue:strToken forKey:@"token"];
        [dictOtpParam setValue:strOtpCode forKey:@"otp_code"];
        
        AFNHelper *afn = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_VERIFY_OTP withParamData:dictOtpParam withBlock:^(id response, NSError *error)
        {
            if(response)
            {
                
            }
        }];
    }
}

- (IBAction)onClickResendOtp:(id)sender {
    
    if([[AppDelegate sharedAppDelegate]connected])
    {
        NSUserDefaults *pref = [NSUserDefaults standardUserDefaults];
        NSString *strId = [pref objectForKey:PREF_USER_ID];
        NSString *strToken = [pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableDictionary *dictResendParam = [[NSMutableDictionary alloc]init];
        
        [dictResendParam setValue:strId forKey:@"id"];
        [dictResendParam setValue:strToken forKey:@"token"];
        
        AFNHelper *afn = [[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
        [afn getDataFromPath:FILE_RESEND_OTP withParamData:dictResendParam withBlock:^(id response, NSError *error)
         {
             if(response)
             {
                 
             }
         }];
    }
}
@end
