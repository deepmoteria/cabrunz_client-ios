//
//  VerificationController.h
//  Cabrunz Client
//
//  Created by My Mac on 10/9/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface VerificationController : BaseVC
@property (weak, nonatomic) IBOutlet UITextField *txtCode;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnResendOtp;



- (IBAction)onClickVerify:(id)sender;
- (IBAction)onClickResendOtp:(id)sender;



@end
