//
//  WriteNoteVC.h
//  TaxiNow
//
//  Created by My Mac on 6/3/15.
//  Copyright (c) 2015 Jigs. All rights reserved.
//

#import "BaseVC.h"

@interface WriteNoteVC : BaseVC<UITextViewDelegate,UIGestureRecognizerDelegate>
{
    NSTimer *timerForCheckReqStatus;
    NSMutableDictionary *driverInfo;
    UITapGestureRecognizer *tapText;
}
@property (weak, nonatomic) IBOutlet UITextView *txtViewNote;
@property (weak, nonatomic) IBOutlet UIButton *btnMakeRequest;
@property (weak, nonatomic) IBOutlet UILabel *lblWriteNoteForDriver;
@property(weak,nonatomic)NSString *strGetSourceAddress;
@property(weak,nonatomic)NSString *strGetDestinationAddress;
@property(weak,nonatomic)NSString *strForLatitude;
@property(weak,nonatomic)NSString *strForLongitude;
@property(weak,nonatomic)NSString *strForDestLatitude;
@property(weak,nonatomic)NSString *strForDestLongitude;
@property(weak,nonatomic)NSString *strAvailId;
@property(weak,nonatomic)NSString *strForTypeId;

@property (weak, nonatomic) IBOutlet UIButton *btnCancel;

@property(weak,nonatomic)NSMutableArray *arrForProps;
@property(weak,nonatomic)NSMutableArray *arrForSubTypeId;

- (IBAction)cancelReqBtnPressed:(id)sender;
- (IBAction)makeRequestBtnPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *revealButtonItem;

@end
